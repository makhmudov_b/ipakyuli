$(document).ready(function () {
    let getCard = $('.main-card');
    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        speed: 500,
        dots: true,
        arrows: false,
    });
    $('.categories__slider').slick({
        variableWidth: true,
        swipeToSlide: true,
        // autoplay: true,
        autoplaySpeed: 2000,
        speed: 500,
        dots: false,
        arrows: false,
    });
    // $('.product-slider').slick({
    //     variableWidth: true,
    //     swipeToSlide: true,
    //     // autoplay: true,
    //     autoplaySpeed: 2000,
    //     speed: 500,
    //     dots: false,
    //     arrows: false,
    // });

    $(".big-cards").slick({
        slidesToShow: 3,
        infinite: true,
        slidesToScroll: 1,
        swipeToSlide: true,
        // autoplay: true,
        speed: 300,
        dots: false,
        variableWidth: true,
        nextArrow: $(this).parent().find('.next-arrow'),
        prevArrow: $(this).parent().find('.prev-arrow'),
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                swipeToSlide: true,
                infinite: true
            }
        },
            {
                breakpoint: 769,
                infinite: true,
                dots: false,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    getCard.slick({
        variableWidth: true,
        // autoplay: true,
        speed: 300,
        swipeToSlide: true,
        infinite: true,
        dots: false,
        arrows: false
        // nextArrow: $(this).parent().find('.next-arrow'),
        // prevArrow: $(this).parent().find('.prev-arrow'),
    })

    $(".login-btn").click(function () {
        $(this).parent().find('.login-hidden').toggleClass("show");
    });
    $(".hamburger").click(function () {
        $('body').toggleClass('toggled');
        $('.left-navigation').toggleClass("show-bar");
        $('.blur').toggleClass('show');
    });
    $(".blur").click(function () {
        $(".left-navigation").removeClass("show-bar");
        $(this).removeClass("show");
        $('body').removeClass('toggled');
    });

    window.onscroll = function showHeader() {
        let header = $(".navbar-fixed");

        if (window.pageYOffset > 100) {
            header.addClass("show__fixed");
        } else {
            header.removeClass("show__fixed");
        }
    };
    $('body').click(function (evt) {
        let hiddenLogin = $('.nav-bottom__btns-account .show');
        if ((!($(evt.target).is('.ignore-item , .nav-bottom__btns-account, .login-hidden , .show , .show form , .show form input')))) {
            if (hiddenLogin.hasClass('show')) hiddenLogin.removeClass('show');
        }
    });
});
