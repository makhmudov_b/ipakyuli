$(function() {
$(".filer_input3").filer({
  limit: null,
  maxSize: null,
  extensions: null,
  changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
  showThumbs: true,
  theme: "dragdropbox",
  templates: {
    box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
    item: `<li class="gallryUploadBlock_item jFiler-item">
    {{fi-image}}
    <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
    </li>`,
    progressBar: '<div class="bar"></div>',
    itemAppendToEnd: true,
    canvasImage: true,
    removeConfirmation: false,
    _selectors: {
      list: '.jFiler-items-list',
      item: '.jFiler-item',
      progressBar: '.bar',
      remove: '.fe.fe-minus'
    }
  },
  dragDrop: {
    dragEnter: null,
    dragLeave: null,
    drop: null,
    dragContainer: null,
  },
  files: null,
  addMore: true,
  allowDuplicates: true,
  clipBoardPaste: true,
  excludeName: null,
  beforeRender: null,
  afterRender: null,
  beforeShow: null,
  beforeSelect: null,
  itemAppendToEnd: true,
  onSelect: null,
  onSelect: null,
  afterShow: function(jqEl, htmlEl, parentEl, itemEl){
    $('.galleryAddElement').hide()
    $('.cloneGalleryAddElement').remove()
    $('.jFiler-items-list').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
  },
  onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
    var filerKit = inputEl.prop("jFiler"),
          file_name = filerKit.files_list[id].name;
      $.post('./php/ajax_remove_file.php', {file: file_name});
      if(filerKit.files_list.length == 1){
        $('.galleryAddElement').show()
      }
  },
  onEmpty: null,
  options: null,
});
$('.uploader').on('click', '.cloneGalleryAddElement', function(e){
  $('.galleryAddElement').trigger('click');
})
})
