<?php

namespace App\Http\Controllers;

use App\Homeland;
use App\HomelandDisease;
use App\Product;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

use App\Setting;
use App\Corona;
use Intervention\Image\ImageManagerStatic as Image;

class SettingController extends Controller
{
    public function get()
    {
        $data = Corona::all();
        return view('backend.corona.index', compact('data'));
    }

    public function create()
    {
        return view('backend.corona.create');
    }

    public function sendNotification()
    {
        $client = new Client();
        $client->post('http://91.204.239.44/broker-api/send', [
            'auth' => ['topinguz', 'TDtfyE1i'],

            'json' => [
                "messages" => [
                    [
                        "recipient" => '977723757',
                        "message-id" => '977723757'.strval(time()),

                        "sms" => [
                            "originator" => "3700",
                            "content" => ["text" => "Вам поступил заказ , перейдите по ссылке : http://ipakyuli.com/dashboard/orders"]
                        ]
                    ]
                ]
            ]
        ]);
        return true;
    }

    public function optimize()
    {
        \Artisan::call('route:cache');
        \Artisan::call('config:cache');
        \Artisan::call('cache:clear');
        \Artisan::call('view:clear');
        \Artisan::call('optimize');
        \Artisan::call('optimize:clear');
        return redirect()->back();
    }

    public function edit($id)
    {
        $data = Corona::findOrFail($id);
        $directory = "uploads/corona/".$id;
        $images = \File::glob($directory."/*.jpg");
        return view('backend.corona.edit', compact('data', 'images'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);
        $corona = Corona::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);
        if ($request->file('image')) {
            $this->createFolder('corona', $corona->id);
            $images = $request->file('image');
            $this->storeImages($images, $corona->id);
        }
        return redirect()->action('SettingController@get')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
        $corona = Corona::findOrFail($id);
        $get_id = $corona->id;
        $corona->update([
            'title' => $request->title,
            'description' => $request->description,
        ]);
        if ($request->file('image')) {
            $checker = $this->cleanDirectory($corona->id);
            if ($checker) {
                $this->createFolder('corona', $get_id);
                $images = $request->file('image');
                $this->storeImages($images, $get_id);
            }
        }
        return redirect()->action('SettingController@get')->with('success', 'Изменения успешно внесены');
    }

    public function createFolder($directory, $coronaId)
    {
        $path = public_path('uploads/'.$directory.'/'.$coronaId);
        $directory_path = public_path('uploads/'.$directory);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($image, $coronaId)
    {
        $filename = microtime().'.jpg';
        $path = public_path('uploads/corona/'.$coronaId.'/'.$filename);
        Image::make($image->getRealPath())->encode('jpg', 100)
            ->fit(330, 200)
            ->save($path);
        return 1;
    }

    public function cleanDirectory($coronaId)
    {
        $pathToDestroy = public_path('uploads/corona/'.$coronaId.'/');
        \File::cleanDirectory($pathToDestroy);
        return 1;
    }

    public function refreshCorona()
    {
        $client = new Client();
        try {
            $response = $client->request('GET', 'https://pomber.github.io/covid19/timeseries.json');
            $response_data = $response->getBody()->getContents();
            $countries = json_decode($response_data, true);
            foreach ($countries as $key => $country) {
                $homeland = Homeland::where('name', $key)->firstOrCreate([
                    'name' => $key
                ]);
                foreach ($country as $disease) {
                    HomelandDisease::where('homeland_id', $homeland->id)
                        ->whereDate('date', $disease['date'])->firstOrCreate([
                            'homeland_id' => $homeland->id,
                            'confirmed' => $disease['confirmed'] * 1,
                            'deaths' => $disease['deaths'] * 1,
                            'recovered' => $disease['recovered'] * 1,
                            'date' => $disease['date']
                        ]);
                }
            }
            return redirect()->action('SettingController@get');
        } catch (ClientException $e) {
            return redirect()->action('SettingController@get')->with('error', 'Failed to fetch ask Khodja');
        }
    }

    public function getCorona($id)
    {
        $homeland = Homeland::findOrFail($id);
        $disease = HomelandDisease::where('homeland_id', $homeland->id)->orderBy('date', 'desc')->get();
        $data = $disease;
        $late = Homeland::first();
        $get_date = HomelandDisease::where('homeland_id', $late->id)->orderBy('date', 'desc')->take(2)->get();
        $disease = HomelandDisease::whereDate('date', $get_date->first()->date)->get();
        $disease_yesterday = HomelandDisease::whereDate('date', $get_date[1]->date)->get();
        $recover = $disease->sum('recovered');
        $world = collect([
            'homeland_id' => 0,
            'confirmed' => $disease->sum('confirmed'),
            'deaths' => $disease->sum('deaths'),
            'recovered' => $recover,
            'added' => $recover - $disease_yesterday->sum('recovered'),
            'date' => $disease->last()->date,
        ]);
        return response()->json(['data' => $data, 'world' => $world]);
    }

    public function getCountries()
    {
        $data = Homeland::all();
        return response()->json($data);
    }

    public function getUseful()
    {
        $data = Corona::all();
        return response()->json($data);
    }

    public function getUsefulById($id)
    {
        $data = Corona::findOrFail($id);
        return response()->json($data);
    }

    public function sendForm(Request $request)
    {
        $name = request('name');
        $height = request('height');
        $phone = request('phone');
        $weight = request('weight');
        $type = request('type') * 1;
        $payment_type = request('payment_type') == 0 ? 'Наличка' : 'Payme';
        switch ($type) {
            case 0:
                $sport = "Меньше 10 тысяч шагов";
                break;
            case 1:
                $sport = "Более 10 тысяч шагов";
                break;
            case 2:
                $sport = "Интенсивный спорт";
                break;
            case 3:
                $sport = "Ежедневный спорт";
                break;
            case 4:
                $sport = "Стабильный 2 раза в день";
                break;
        }
        $botToken = '1161634369:AAH9BCvTJDH-_FSz0nxogO1yPzsJGOF1-IQ';
        $chatID = '-1001182634286';
        $pesan = urlencode('Имя: '.$name.', Телефон:'.$phone.',  Рост : '.$height.' , Вес: '.$weight.' ,  Уровень активности : '.$sport.' ,  Оплата : '.$payment_type);

        $token = "bot".$botToken;

        $url = "https://api.telegram.org/$token/sendMessage?chat_id=$chatID&text=$pesan";

        $ch = curl_init();

        $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
        );

        curl_setopt_array($ch, $optArray);
        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err <> "") {
            return $err;
        } else {
            return $result;
        }
    }
}
