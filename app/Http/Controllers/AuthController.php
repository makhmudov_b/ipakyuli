<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Phone;
use GuzzleHttp\Client;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'username' => 'required',
        ]);
        \Auth::user()->update([
            'name' => $request->username,
        ]);
        return redirect()->action('PageController@process')->with('success', 'Успешно');
    }

    public function auth(Request $request)
    {
        $request->validate([
            'phone' => 'required',
            'password' => 'required',
        ]);
        $checkForExist = User::where('phone', request('phone'))->first();
        Phone::where([['phone', request('phone')], ['code', request('password')]])->firstOrFail();
        if (!$checkForExist) {
            $user = User::create([
                'phone' => request('phone'),
                'password' => bcrypt(request('password')),
            ]);
        } else {
            $user = $checkForExist;
        }
        auth()->login($user);
        if ($user->name) {
            return redirect()->action('PageController@process')->with('success', 'Успешно');
        }
        return redirect()->action('PageController@setName');
    }

    public function login()
    {
        $user = User::where(['phone' => '998' . request('phone')])->firstOrFail();
        if (request('password') == $user->password) {
            auth()->login($user);
            return redirect()->action('PageController@index')->with('success', 'Успешно , зарегистрировались');
        } else {
            return redirect()->back()->with('error', 'Не правильный код');
        }
    }

    public function verification(Request $request)
    {
        $request->validate([
            'phone' => 'required'
        ]);
        $password = rand(1000, 9999);
        $success = $this->send_code($request->phone, $password);
        Phone::updateOrCreate(['phone' => $request->phone], [
            'phone' => $request->phone,
            'code' => $password,
        ]);
        if (!$success) {
            return redirect()->back()->with('error', 'Смс сервис не работает');
        }
        session()->put('phone', $request->phone);
        return redirect()->action('PageController@confirm')->with('success', 'Код отправлен на ваш телефон');
    }

    public function send_code($phone, $password)
    {
        $verify_password = $password;
        $client = new Client();
        $client->post('http://91.204.239.44/broker-api/send', [
            'auth' => ['topinguz', 'TDtfyE1i'],
            'json' => [
                "messages" => [
                    [
                        "recipient" => $phone,
                        "message-id" => $phone . strval(time()),

                        "sms" => [
                            "originator" => "3700",
                            "content" => ["text" => "Ваш код подтверждения: " . $verify_password . '. Наберите его в поле ввода.']
                        ]
                    ]
                ]
            ]
        ]);
        return true;
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->action('PageController@index')->with('success', 'Успешно вышли из профиля');
    }
}
