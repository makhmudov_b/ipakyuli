<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Shipping;
use Auth;

class ShippingController extends Controller
{
    public function index()
    {
        $data = Shipping::all();
        return view('backend.shipping.index', compact('data'));
    }

    public function create()
    {
        $city = City::all();
        return view('backend.shipping.create', compact('city'));
    }

    public function edit($id)
    {
        $data = Shipping::findOrFail($id);
        $city = City::all();
        return view('backend.shipping.edit', compact('data', 'city'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'from_id' => 'required',
            'to_id' => 'required',
        ]);
        Shipping::create($request->except('_token'));
        return redirect()->action('ShippingController@index')->with('success', 'Успешно добавлено');
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'from_id' => 'required',
            'to_id' => 'required',
        ]);
        Shipping::findOrFail($id)->update($request->except('_token','method'));
        return redirect()->action('ShippingController@index')->with('success', 'Успешно');
    }
}
