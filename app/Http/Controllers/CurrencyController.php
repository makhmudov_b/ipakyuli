<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Currency;

class CurrencyController extends Controller
{
    public function get()
    {
        $data = Currency::get();
        return view('backend.currency.index', compact('data'));
    }

    public function update()
    {
        $client = new Client();
        $res = $client->request('GET', 'https://nbu.uz/exchange-rates/json/');

        if ($res->getStatusCode() == 200) {
            $currencies = $res->getBody()->getContents();
            $currencies_decoded = json_decode($currencies, true);
            $getFirst = Currency::first();
            if (!$getFirst) {
                Currency::create(['value' => (float) $currencies_decoded[23]['nbu_cell_price']]);
            } else {
                $getFirst->update(['value' => (float) $currencies_decoded[23]['nbu_cell_price']]);
            }
            return back()->with('success', 'Успешно');
        } else {
            return back()->with('error', 'currency error');
        }
    }
}
