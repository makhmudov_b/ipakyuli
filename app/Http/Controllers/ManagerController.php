<?php

namespace App\Http\Controllers;

use App\Carrier;
use App\Role;
use App\Vendor;
use Illuminate\Http\Request;
use App\User;
use App\City;

class ManagerController extends Controller
{
    public function index()
    {
        $data = User::whereHas('roles', function ($query) {
            $query->where('type', 'manager');
        })->get();
        return view('backend.users.index', compact('data'));
    }

    public function users()
    {
        $data = User::all();
        return view('backend.users.index', compact('data'));
    }

    public function carriers()
    {
        $data = User::whereHas('roles', function ($query) {
            $query->where('type', 'carrier');
        })->get();
        $carrier = 1;
        return view('backend.users.index', compact('data', 'carrier'));
    }

    public function create()
    {
        $data = Role::all();
        $vendors = Vendor::all();
        return view('backend.users.create', compact('data', 'vendors'));
    }

    public function createCarrier()
    {
        $data = Role::all();
        $carriers = Carrier::all();
        return view('backend.users.carrier', compact('data', 'carriers'));
    }

    public function edit($id)
    {
        $data = User::findOrFail($id);
        $role = Role::all();
        $vendors = Vendor::all();
        return view('backend.users.edit', compact('data', 'role', 'vendors'));
    }

    public function show($id)
    {
        $data = User::findOrFail($id);
        return view('backend.users.show', compact('data'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required',
            'phone' => 'required|unique:users',
        ]);
        $role = Role::where('type', request('role'))->first();
        $data = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'phone' => request('phone'),
            'vendor_id' => request('vendor_id'),
            'password' => bcrypt(request('password'))
        ]);
        $data->roles()->attach($role);
        return redirect()->action('ManagerController@index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $role = Role::where('type', request('role'))->first();
        $user = User::find($id);
        if ($request->password != null) {
            $user->update(['password' => bcrypt(request('password'))]);
        }
        $user->update($request->except('password'));
        $user->roles()->sync($role);

        return redirect()->action('ManagerController@index')->with('success', 'Изменения успешно внесены');
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->action('ManagerController@index')->with('success', 'Успешно удален');
    }
}
