<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;
use App\Product;
use App\ProductSize;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;

class ProductSizeController extends Controller
{
    public function index($vendorAlias, $productAlias)
    {
        $product = Product::where('alias', $productAlias)->with('vendor')->firstOrFail();
        $vendor = $product->vendor;
        $data = ProductSize::where('product_id', $product->id)->get();
        return view('backend.product_size.index', compact('data', 'vendor', 'product'));
    }

    public function create($vendorAlias, $productAlias)
    {
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $product = Product::where('alias', $productAlias)->firstOrFail();
        return view('backend.product_size.create', compact('vendor', 'product'));
    }

    public function edit($vendorAlias, $productAlias, $productSizeId)
    {
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $product = Product::where('alias', $productAlias)->firstOrFail();
        $data = ProductSize::findOrFail($productSizeId);
        $directory = "uploads/product_size/".$productSizeId;
        $images = \File::glob($directory."/*.jpg");
        return view('backend.product_size.edit', compact('data', 'product', 'images', 'vendor'));
    }

    public function store(Request $request, $vendorAlias, $productAlias)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $product = Product::where('alias', $productAlias)->firstOrFail();
        $request['product_id'] = $product->id;
        $product_size = ProductSize::create($request->except('_token', 'image'));
        if ($request->file('image')) {
            $this->createFolder('product_size', $product_size->id);
            $images = $request->file('image');
            $this->storeImages($images, $product_size->id);
        }
        return redirect()->action('ProductSizeController@index', [$vendorAlias, $productAlias])->with('success',
            'Успешно добавлено');
    }

    public function update(Request $request, $vendorAlias, $productAlias, $productSizeId)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $product_size = ProductSize::findOrFail($productSizeId);
        $product_size->update($request->except('_token', 'image'));
        if ($request->file('image')) {
            $this->createFolder('product_size', $product_size->id);
            $images = $request->file('image');
            $this->storeImages($images, $product_size->id);
        }
        return redirect()->action('ProductSizeController@index', [$vendorAlias, $productAlias])->with('success',
            'Успешно добавлено');
    }

    public function createFolder($directory, $productSizeId)
    {
        $path = public_path('uploads/'.$directory.'/'.$productSizeId);
        $directory_path = public_path('uploads/'.$directory);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($images, $productSizeId)
    {
        foreach ($images as $key => $image) {
            $filename = $key + strtotime("now").'.jpg';
            $path = public_path('uploads/product_size/'.$productSizeId.'/'.$filename);
            Image::make($image->getRealPath())->encode('jpg', 60)
                ->fit(945, 630)
                ->save($path);
        }
        return 1;
    }

    public function removeImage($productSizeId)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/product_size/'.$productSizeId.'/'.$file_name);
        \File::delete($pathToDestroy);
        return response('okay', 200);
    }

    public function switch(Request $request, $vendorAlias, $productAlias, $productSizeId)
    {
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $product = Product::where('alias', $productAlias)->firstOrFail();
        $product_size = ProductSize::findOrFail($productSizeId);
        if ($vendor->id == $product->vendor_id && $product_size->product_id == $product->id) {
            $product_size->update([
                'available' => !$product_size->available
            ]);
            return redirect()->action('DashboardController@index')->with('success', 'Изменения успешно внесены');
        } else {
            return redirect()->action('DashboardController@index')->with('error', 'Не получилось изменить');
        }
    }
    public function delete($vendorAlias, $productAlias, $productSizeId)
    {
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $product = Product::where('alias', $productAlias)->firstOrFail();
        $product_size = ProductSize::findOrFail($productSizeId);
        if (!((Auth::user()->vendor_id == $vendor->id || Auth::user()->hasRole('admin')) || $product->vendor_id == $vendor->id || $product_size->product_id == $product->id)) {
            return redirect()->back()->with('error', 'У вас нет прав на просмотр данного продукта');
        } else {
            $product_size->type()->delete();
            $product_size->delete();
            return redirect()->action('ProductSizeController@index',
                [$vendorAlias, $productAlias])->with('success', 'Успешно');
        }
    }
}
