<?php

namespace App\Http\Controllers;

use App\ProductSizeType;
use App\Vendor;
use Illuminate\Http\Request;
use App\ProductSize;
use App\Product;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;

class ProductSizeTypeController extends Controller
{
    public function index($vendorAlias, $productAlias, $productSizeId)
    {
        $data = ProductSizeType::where('product_size_id', $productSizeId)->with('size.product.vendor')->get();
        $product = Product::where('alias', $productAlias)->firstOrFail();
        $product_size = ProductSize::where('id', $productSizeId)->with('product.vendor')->firstOrFail();
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        if (!((Auth::user()->vendor_id == $vendor->id || Auth::user()->hasRole('admin')) || $product->vendor_id == $vendor->id || $product_size->product_id == $product->id)) {
            return redirect()->action('DashboardController@index')->with('error',
                'У вас нет прав на просмотр данного продукта');
        }
        return view('backend.size_type.index', compact('data', 'vendor', 'product', 'product_size'));
    }

    public function create($vendorAlias, $productAlias, $productSizeId)
    {
        $product_size = ProductSize::where('id', $productSizeId)->with('product.vendor')->firstOrFail();
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $product = Product::where('alias', $productAlias)->firstOrFail();
        if (!((Auth::user()->vendor_id == $vendor->id || Auth::user()->hasRole('admin')) || $product->vendor_id == $vendor->id || $product_size->product_id == $product->id)) {
            return redirect()->action('DashboardController@index')->with('error',
                'У вас нет прав на просмотр данного продукта');
        }
        return view('backend.size_type.create', compact('vendor', 'product', 'product_size'));
    }

    public function edit($vendorAlias, $productAlias, $productSizeId, $productTypeSizeId)
    {
        $data = ProductSizeType::where('id', $productTypeSizeId)->with('size.product.vendor')->firstOrFail();
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $product = Product::where('alias', $productAlias)->firstOrFail();
        $product_size = ProductSize::where('id', $productSizeId)->with('product.vendor')->firstOrFail();
        $directory = "uploads/product_size_type/".$productTypeSizeId;
        $images = \File::glob($directory."/*.jpg");
        return view('backend.size_type.edit', compact('data', 'product', 'product_size', 'images', 'vendor'));
    }

    public function store(Request $request, $vendorAlias, $productAlias, $productSizeId)
    {
        $request->validate([
            'name_ru' => 'required',
            'name_uz' => 'required',
        ]);
        $request['product_size_id'] = $productSizeId;
        $price = $request['pricing'];
        $amount = $request['amount'];
        $produce = $request['produce'];
        $price_amount = [];
        foreach ($price as $key => $item) {
            array_push($price_amount, [$amount[$key], $item, $produce[$key]]); /// [amount,price , produce-days]
        }
        $request['price'] = json_encode($price_amount);
        $product_size_type = ProductSizeType::create($request->except('_token', 'image', 'pricing', 'amount',
            'produce'));
        if ($request->file('image')) {
            $this->createFolder('product_size_type', $product_size_type->id);
            $images = $request->file('image');
            $this->storeImages($images, $product_size_type->id);
        }
        return redirect()->action('ProductSizeTypeController@index',
            [$vendorAlias, $productAlias, $productSizeId])->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $vendorAlias, $productAlias, $productSizeId, $productSizeTypeId)
    {
        $request->validate([
            'name_ru' => 'required',
            'name_uz' => 'required',
            'amount' => 'required'
        ]);
        $request['product_size_id'] = $productSizeId;
        $price = $request['pricing'];
        $amount = $request['amount'];
        $produce = $request['produce'];
        $price_amount = [];
        foreach ($price as $key => $item) {
            array_push($price_amount, [$amount[$key], $item, $produce[$key]]); /// [amount,price , produce-days]
        }
        $request['price'] = json_encode($price_amount);
        $product_size_type = ProductSizeType::findOrFail($productSizeTypeId);
        $product_size_type->update($request->except('_token', 'image', 'pricing', 'amount', 'produce'));
        if ($request->file('image')) {
            $this->createFolder('product_size_type', $product_size_type->id);
            $images = $request->file('image');
            $this->storeImages($images, $product_size_type->id);
        }
        return redirect()->action('ProductSizeTypeController@index',
            [$vendorAlias, $productAlias, $productSizeId])->with('success', 'Успешно добавлено');
    }

    public function createFolder($directory, $productSizeTypeId)
    {
        $path = public_path('uploads/'.$directory.'/'.$productSizeTypeId);
        $directory_path = public_path('uploads/'.$directory);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($images, $vendorAlias)
    {
        foreach ($images as $key => $image) {
            $filename = $key + strtotime("now").'.jpg';
            $path = public_path('uploads/product_size_type/'.$vendorAlias.'/'.$filename);
            Image::make($image->getRealPath())->encode('jpg', 60)
                ->fit(945, 630)
                ->save($path);
        }
        return 1;
    }

    public function removeImage($id)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/product_size_type/'.$id.'/'.$file_name);
        \File::delete($pathToDestroy);
        return response('okay', 200);
    }

    public function switch($id)
    {
        $product_size = ProductSizeType::where('id', $id)->with('size.product.vendor')->firstOrFail();
        $product_size->update([
            'available' => !$product_size->available
        ]);
        return redirect()->action('DashboardController@index')->with('success', 'Изменения успешно внесены');
    }

    public function delete($vendorAlias, $productAlias, $productSizeId, $productTypeSizeId)
    {
        $data = ProductSizeType::where('id', $productTypeSizeId)->firstOrFail();
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $product = Product::where('alias', $productAlias)->firstOrFail();
        $product_size = ProductSize::where('id', $productSizeId)->firstOrFail();
        if (!((Auth::user()->vendor_id == $vendor->id || Auth::user()->hasRole('admin')) || $product->vendor_id == $vendor->id || $product_size->product_id == $product->id)) {
            return redirect()->back()->with('error', 'У вас нет прав на просмотр данного продукта');
        } else {
            $data->delete();
            return redirect()->action('ProductSizeTypeController@index',
                [$vendorAlias, $productAlias, $productSizeId])->with('success', 'Успешно');
        }
    }
}
