<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\User;
use Carbon\Carbon;
use App\Vendor;
use App\Category;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;

class VendorController extends Controller
{
    public function index()
    {
        if(Auth::user()->hasRole('admin')) {
            $data = Vendor::all();
        }
        else if(Auth::user()->hasRole('manager')){
            $data = Vendor::where('user_id',Auth::user()->id)->get();
        }
        else{
            $data = null;
        }
        return view('backend.vendors.index',compact('data'));
    }
    public function create()
    {
        $country = Country::all();
        $city = City::all();
        $categories = Category::whereNull('parent_id')->with('sub_category','parent_category')->get();
        $user = User::all();

        return view('backend.vendors.create', compact('country','city','categories','user') );
    }
    public function edit($alias)
    {
        $data = Vendor::where('alias',$alias)->firstOrFail();
        $city = City::all();
        $directory = "uploads/vendor/".$data->id;
        $images = \File::glob($directory . "/*.jpg");
        $country = Country::all();
        $categories = Category::whereNull('parent_id')->with('sub_category','parent_category')->get();
        return view('backend.vendors.edit', compact('data','country','city','images','categories'));
    }
    public function switch(Request $request, $alias)
    {
        $vendor = Vendor::where('alias',$alias)->firstOrFail();
        $vendor->update([
            'enabled'=> !$vendor->enabled
        ]);
        return redirect()->action('DashboardController@index')->with('success','Изменения успешно внесены');
    }
    public function store(Request $request)
    {
        $request->validate([
            'country_id'=>'required',
            'city_id'=>'required',
            'name'=>'required',
            'description_uz'=> 'required',
            'description_ru'=> 'required',
            'address'=> 'required',
            'phone'=> 'required',
            'open_date'=> 'required',
            'category_id'=> 'required',
            'bank'=> 'required',
            'bank_account'=> 'required',
            'inn'=> 'required',
            'out_account'=> 'required',
            'swift'=> 'required',
            'mfo'=> 'required',
            'oked'=> 'required',
            'nds'=> 'required',
            'additional_bank'=> 'required',
            'additional_swift'=> 'required',
            'bank_no'=> 'required'
        ]);
        if(!$request['user_id']) $request['user_id'] = Auth::user()->id;
        else $request['user_id'] = User::findOrFail($request['user_id'])->id;
        $request['open_date'] = Carbon::parse($request['open_date']);

        $vendor = Vendor::create($request->except('_token','image','category_id','jfiler-items-exclude-image-0'));
        $vendor->categories()->attach($request->category_id);

        if($request->file('image')){
            $this->createFolder('vendor',$vendor->id);
            $images = $request->file('image');
            $this->storeImages($images,$vendor->id);
        }
        return redirect()->action('VendorController@index')->with('success','Успешно добавлено');
    }
    public function update(Request $request,$alias)
    {
        $request->validate([
            'country_id'=>'required',
            'city_id'=>'required',
            'name'=>'required',
            'description_uz'=> 'required',
            'description_ru'=> 'required',
            'address'=> 'required',
            'phone'=> 'required',
            'open_date'=> 'required',
            'category_id'=> 'required',
            'bank'=> 'required',
            'bank_account'=> 'required',
            'inn'=> 'required',
            'out_account'=> 'required',
            'swift'=> 'required',
            'mfo'=> 'required',
            'oked'=> 'required',
            'nds'=> 'required',
            'additional_bank'=> 'required',
            'additional_swift'=> 'required',
            'bank_no'=> 'required'
        ]);
        $vendor = Vendor::where('alias',$alias)->firstOrFail();
        $vendor->update($request->except('_token','image','user_id','category_id','_method','jfiler-items-exclude-image-0'));
        if($request->category_id){
            $vendor->categories()->sync($request->category_id);
        }
        if($request->file('image')){
            $this->createFolder('vendor',$vendor->id);
            $images = $request->file('image');
            $this->storeImages($images,$vendor->id);
        }
        return redirect()->action('VendorController@index')->with('success','Успешно добавлено');
    }
    public function createFolder($directory,$vendorId)
    {
        $path = public_path('uploads/'.$directory.'/'.$vendorId);
        $directory_path = public_path('uploads/'.$directory);
        if(!\File::isDirectory($directory_path)){
            \File::makeDirectory($directory_path,0777,true,true);
        }
        if(!\File::isDirectory($path)){
            \File::makeDirectory($path,0777,true,true);
        }
        return 1;
    }
    public function storeImages($images,$vendorId)
    {
        foreach($images as $key => $image){
            $filename = $key+strtotime("now") .'.jpg';
            $path = public_path('uploads/vendor/'.$vendorId.'/'. $filename);
            Image::make($image->getRealPath())->encode('jpg', 60)
            ->fit(5400, 400)
            ->save($path);
        }
        return 1;
    }
    public function removeImage($id)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/vendor/'.$id.'/'.$file_name);
        \File::delete($pathToDestroy);
        return response('okay',200);
    }
}
