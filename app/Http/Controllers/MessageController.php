<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use App\Message;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;
use GuzzleHttp\Client;
class MessageController extends Controller
{
    public function list()
    {
        $user = Auth::user();
        $messages = Message::where('user_id', $user->id)->with('order.order_item.product')->orderBy('created_at',
            'DESC')->get();
        $messages = $messages->unique('order_id');
        return response()->json(['data' => $messages, 'statusCode' => 200], 200);
    }

    public function show($id)
    {
        $order = Order::where('id', $id)->with('order_item')->firstOrFail();
        $messages = Message::orWhere([['order_id', '=', $order->id]])->with('user')->get();
        return view('backend.message.index', compact('order', 'messages'));
    }

    public function store($productId)
    {
        request()->validate([
            'count' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
//            'size_id' => 'required',
//            'type_id' => 'required',
        ]);
        $product = Product::where('id', $productId)->firstOrFail();
        $user = Auth::user();
        $infoName = '';
        if (!is_null(request('size_id')) && $product->getTypeAttribute() == 'structured') {
            $size = $product->size()->where('id', request('size_id') * 1)->first();
            if (!is_null(request('type_id') && !is_null($size))) {
                $type = $size->type() > where('id', request('type_id') * 1)->first();
                $infoName = $size->name.' '.$type->name;
            }
        }
        $order = Order::create([
            'vendor_id' => $product->vendor_id * 1,
            'city_id' => request('city_id') * 1,
            'user_id' => Auth::user()->id,
            'phone' => Auth::user()->phone,
            'price' => 0,
        ]);
        OrderItem::create([
            'product_id' => $product->id,
            'order_id' => $order->id,
            'name' => $product->name.$infoName,
            'amount' => request('count') * 1,
            'price' => 0,
        ]);
        Message::create([
            'user_id' => $user->id,
            'order_id' => $order->id,
            'receiver_id' => $order->vendor_id,
            'body' => 'Здравствуйте давайте обсудим цену'
        ]);
        return redirect()->action('PageController@message', $order->id);
    }

    public function send()
    {
        request()->validate([
            'body' => 'required',
            'order_id' => 'required',
        ]);

        $user = Auth::user();
        $order = Order::findOrFail(request('order_id') * 1);
        $receiver_id = null;
        if ($order->vendor_id != $user->vendor_id) {
            $receiver_id = $order->vendor_id;
            $this->sendNotification($order->vendor->phone);
        } else {
            $receiver_id = \request('receiver_id') * 1;
        }
        $message = Message::create([
            'user_id' => $user->id,
            'order_id' => $order->id,
            'receiver_id' => $receiver_id,
            'body' => request('body')
        ]);
        if (request()->file('image')) {
            $this->createFolder('message', $message->id);
            $images = request()->file('image');
            $this->storeImages($images, $message->id);
        }
        return redirect()->back();
    }

    public function createFolder($directory, $messageId)
    {
        $path = public_path('uploads/'.$directory.'/'.$messageId);
        $directory_path = public_path('uploads/'.$directory);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }
    public function sendNotification()
    {
        $client = new Client();
        $client->post('http://91.204.239.44/broker-api/send', [
            'auth' => ['topinguz', 'TDtfyE1i'],

            'json' => [
                "messages" => [
                    [
                        "recipient" => '977723757',
                        "message-id" => '977723757'.strval(time()),

                        "sms" => [
                            "originator" => "3700",
                            "content" => ["text" => "Вам поступил заказ , перейдите по ссылке : http://ipakyuli.com/dashboard/orders"]
                        ]
                    ]
                ]
            ]
        ]);
        return true;
    }
    public function storeImages($image, $messageId)
    {
        $filename = strtotime("now").'.jpg';
        $path = public_path('uploads/message/'.$messageId.'/'.$filename);
        Image::make($image->getRealPath())->encode('jpg', 60)->save($path);
        return 1;
    }
}
