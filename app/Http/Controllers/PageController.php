<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Category;
use App\City;
use App\Collection;
use App\Country;
use App\Order;
use App\ShippingOffer;
use App\Vendor;
use Illuminate\Http\Request;
use App\Product;
use App\Message;
use GuzzleHttp\Client;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class PageController extends Controller
{
    public function index()
    {
        $banners = Banner::latest()->take(10)->get();
        $categories = Category::where('main', 1)->whereNull('parent_id')->take(8)->get();
        $collections = Collection::with('products.vendor')->take(4)->get();
        $hots = Product::withCount('order_item')->orderBy('order_item_count', 'DESC')->take(10)->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.index', compact('categories', 'collections', 'banners', 'all_categories', 'hots'));
    }

    public function collection()
    {
        $categories = Category::where('main', 1)->whereNull('parent_id')->take(8)->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        $collections = Collection::all();
        return view('frontend.collection', compact('categories', 'all_categories', 'collections'));
    }

    public function showCollection($id)
    {
        $category = Collection::with('products')->findOrFail($id);
        $products = $category->products;
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.showCollection', compact('products', 'category', 'categories', 'all_categories'));
    }

    public
    function authorization()
    {
        $categories = Category::where('main', 1)->whereNull('parent_id')->take(8)->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.authorization', compact('categories', 'all_categories'));
    }

    public
    function confirm()
    {
        $phone = \Session::get('phone');
        if ($phone) {
            redirect()->action('PageController@authorization');
        }
        $categories = Category::where('main', 1)->whereNull('parent_id')->take(8)->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.confirm', compact('categories', 'all_categories', 'phone'));
    }

    public
    function setName()
    {
        $categories = Category::where('main', 1)->whereNull('parent_id')->take(8)->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.authName', compact('categories', 'all_categories'));
    }

    public
    function top()
    {
        $categories = Category::where('main', 1)->whereNull('parent_id')->take(8)->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        $products = Product::withCount('order_item')->orderBy('order_item_count', 'DESC')->take(10)->get();
        return view('frontend.top', compact('categories', 'products', 'all_categories'));
    }

    public
    function showVendor($alias)
    {
        $categories = Category::where('main', 1)->whereNull('parent_id')->take(8)->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        $vendor = Vendor::where('alias', $alias)->with('categories', 'product')->firstOrFail();
        $products = $vendor->product;
        $related = $products->map->category->unique();
        return view('frontend.vendor', compact('categories', 'all_categories', 'related', 'vendor', 'products'));
    }

    public
    function company()
    {
        $categories = Category::where('main', 1)->whereNull('parent_id')->take(8)->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.company', compact('categories', 'all_categories'));
    }

    public
    function basket()
    {
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $countries = Country::all();
        $cities = City::all();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.basket', compact('categories', 'countries', 'cities', 'all_categories'));
    }

    public
    function showProduct($vendorAlias, $productAlias)
    {
        $product = Product::where('alias', $productAlias)->with('vendor.product', 'size.type')->where('enabled',
            1)->whereHas('vendor', function ($query) {
            $query->where('enabled', 1);
        })->firstOrFail();
        $product->size->each->setAppends(['image']);
        $directory = "uploads/product/" . $product->id;
        $images = \File::glob($directory . "/*.jpg");
        $images = count($images) ? $images : [asset('img/no-photo.png')];
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        $countries = Country::all();
        $cities = City::all();
        return view('frontend.show',
            compact('product', 'images', 'countries', 'cities', 'categories', 'all_categories'));
    }

    public
    function search()
    {
        $keyword = \request('keyword');
        $category_id = \request('category_id');
        $vendor_id = \request('vendor_id');

        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        $vendors = Vendor::where('enabled', 1)->get();
        $products = Product::orderBy('created_at')->whereHas('vendor', function ($query) {
            $query->where('enabled', 1);
        })->where('enabled', 1);;
        if ($keyword) {
            $products = $products->where('name_' . \App::getLocale(), 'like',
                '%' . $keyword . '%');
        }
        if (is_numeric($category_id)) {
            $products->where('category_id', $category_id);
        }
        if (is_numeric($vendor_id)) {
            $products->where('vendor_id', $vendor_id);
        }
        $products = $products->paginate(24);
        return view('frontend.search',
            compact('category_id', 'vendor_id', 'products', 'keyword', 'categories', 'all_categories', 'vendors'));
//       if (isset($request->minimum_price) && isset($request->maximum_price)) {
//           $products->whereBetween('p.price', [$request->minimum_price, $request->maximum_price]);
//       }
//       if (isset($request->brand)) {
//           $products->whereIn('p.brand_id', $request->brand);
//       }
//       if (isset($request->cat)) {
//           $products->whereIn('p.category_id', $request->cat);
//       }

    }

    public
    function category($alias)
    {
        $category = Category::where('alias', $alias)->firstOrFail();
        $sub_id = $category->sub_category()->pluck('id')->toArray();
        array_push($sub_id, $category->id);
        $categories = array_unique($sub_id);
        $products = Product::whereIn('category_id', $categories)->where('enabled', 1)->whereHas('vendor',
            function ($query) {
                $query->where('enabled', 1);
            })->get();
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.showCategory', compact('products', 'category', 'categories', 'all_categories'));
    }

    public
    function categoryAll()
    {
        $categories = Category::whereNull('parent_id')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.category', compact('categories', 'all_categories'));
    }

    public
    function order()
    {
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.order', compact('categories', 'all_categories'));
    }

    public
    function personal()
    {
        if (!\Auth::check()) {
            return redirect()->action('PageController@auth');
        }
        $data = \Auth::user();
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.personal', compact('data', 'categories', 'all_categories'));
    }

    public
    function register()
    {
        if (\Auth::check()) {
            return redirect()->action('PageController@index');
        }
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.register', compact('categories', 'all_categories'));
    }

    public
    function auth()
    {
        if (\Auth::check()) {
            return redirect()->action('PageController@index');
        }
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.auth', compact('categories', 'all_categories'));
    }

    public
    function process()
    {
        if (!\Auth::check()) {
            return redirect()->action('PageController@auth');
        }
        $orders = \Auth::user()->order()->where('city_id', '!=', null)->get();
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.process', compact('orders', 'categories', 'all_categories'));
    }

    public
    function messages()
    {
        $user = \Auth::user();
        $messages = Message::where('user_id', $user->id)->with('order.user')->orderBy('created_at', 'DESC')->get();
        if ($messages) {
            $messages = $messages->unique('order_id');
        }
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.messages', compact('messages', 'categories', 'all_categories'));
    }

    public
    function message($id)
    {
        $user = \Auth::user();
        $order = Order::where('id', $id)->with('order_item')->firstOrFail();
        $messages = Message::where('order_id', $id)->get();
        if (!count($messages)) {
            Message::create([
                'user_id' => $user->id,
                'order_id' => $order->id,
                'receiver_id' => $order->vendor_id,
                'body' => 'Здравствуйте давайте обсудим цену'
            ]);
            $messages = Message::where('order_id', $id)->get();
        }
        $categories = Category::whereNull('parent_id')->take(8)->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.message', compact('categories', 'messages', 'order', 'all_categories'));
    }

    public
    function about()
    {
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.about', compact('categories', 'all_categories'));
    }

    public
    function shipping()
    {
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.shipping', compact('categories', 'all_categories'));
    }

    public
    function rules()
    {
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.rules', compact('categories', 'all_categories'));
    }

    public
    function confidential()
    {
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.confidential', compact('categories', 'all_categories'));
    }

    public
    function policy()
    {
        $categories = Category::take(8)->whereNull('parent_id')->with('sub_category')->get();
        $all_categories = Category::whereNotIn('id',
            $categories->pluck('id'))->whereNull('parent_id')->with('sub_category')->get();
        return view('frontend.policy', compact('categories', 'all_categories'));
    }

    public
    function basketProducts()
    {
        $products = request('products');
        $products = json_decode($products, true);
        $products_ids = [];
        foreach ($products as $product) {
            array_push($products_ids, $product[1]);
        }
        $getProds = Product::whereIn('id', $products_ids)->with('size.type', 'vendor')->get();
        return response()->json($getProds);
    }

    public
    function updateOrder($id, $type)
    {
        $order = \Auth::user()->order()->findOrFail($id);
        // type - 0 v ojidanii oplati 1 - podtevrjden 2 - dostavshik vibran 3 - v puti 4 - dostavlen
        $type = $type * 1;
        if ($type !== 1) {
            $order->update(['status' => $type + 1]);
        } else {
            $order->update(['status' => $type + 1, 'shipping_price' => 16500]);
        }
        return redirect()->back()->with('success', 'Успешно');
    }

    public
    function chooseCarrier($id)
    {
        \request()->validate([
            'offer_id' => 'required'
        ]);
        $order = \Auth::user()->order()->findOrFail($id);
        if (request('offer_id') != -1) {
            $offer = ShippingOffer::where('id', \request('offer_id'))->firstOrFail();
            $carrier = $offer->carrier_id;
            $this->sendNotification();
            $order->update([
                'status' => $order->status * 1 + 1, 'shipping_price' => $offer->price, 'carrier_id' => $carrier
            ]);
        } else {

            $order->update([
                'status' => $order->status * 1 + 1,
            ]);
        }

        return redirect()->back()->with('success', 'Успешно');
    }

    public
    function sendNotification()
    {
        $client = new Client();
        $client->post('http://91.204.239.44/broker-api/send', [
            'auth' => ['topinguz', 'TDtfyE1i'],

            'json' => [
                "messages" => [
                    [
                        "recipient" => '977723757',
                        "message-id" => '977723757' . strval(time()),

                        "sms" => [
                            "originator" => "3700",
                            "content" => ["text" => "Вам поступил заказ на доставку , перейдите по ссылке : http://ipakyuli.com/dashboard/orders"]
                        ]
                    ]
                ]
            ]
        ]);
        return true;
    }
}
