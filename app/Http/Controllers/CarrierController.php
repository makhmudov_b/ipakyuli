<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\Carrier;
use Auth;
use App\User;
use App\Country;
use App\City;
use Intervention\Image\ImageManagerStatic as Image;


class CarrierController extends Controller
{
    public function index()
    {
        $data = Carrier::all();
        return view('backend.carriers.index', compact('data'));
    }

    public function create()
    {
        $country = Country::all();
        $city = City::all();
        $user = User::all()->except(1);
        return view('backend.carriers.create', compact('country', 'city', 'user'));
    }

    public function edit($id)
    {
        $data = Carrier::findOrFail($id);
        $city = City::all();
        $directory = "uploads/vendor/".$id;
        $images = \File::glob($directory."/*.jpg");
        $country = Country::all();
        return view('backend.carriers.edit', compact('data', 'country', 'city', 'images'));
    }

    public function switch(Request $request, $id)
    {
        $vendor = Carrier::findOrFail($id);
        $vendor->update([
            'enabled' => !$vendor->enabled
        ]);
        return redirect()->action('DashboardController@index')->with('success', 'Изменения успешно внесены');
    }

    public function store(Request $request)
    {
        $request->validate([
            'country_id' => 'required',
            'city_id' => 'required',
            'name' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'open_date' => 'required',
        ]);

        Carrier::create($request->except('_token', 'image'));
        return redirect()->action('CarrierController@index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'country_id' => 'required',
            'city_id' => 'required',
            'name' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'open_date' => 'required',
        ]);

        Carrier::findOrFail($id)->update($request->except('_token', 'image'));
        return redirect()->action('CarrierController@index')->with('success', 'Успешно');
    }

    public function createFolder($directory, $vendorId)
    {
        $path = public_path('uploads/'.$directory.'/'.$vendorId);
        $directory_path = public_path('uploads/'.$directory);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($images, $vendorId)
    {
        foreach ($images as $key => $image) {
            $filename = $key + strtotime("now").'.jpg';
            $path = public_path('uploads/vendor/'.$vendorId.'/'.$filename);
            Image::make($image->getRealPath())->encode('jpg', 60)
                ->fit(945, 630)
                ->save($path);
        }
        return 1;
    }

    public function removeImage($id)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/vendor/'.$id.'/'.$file_name);
        \File::delete($pathToDestroy);
        return response('okay', 200);
    }
}
