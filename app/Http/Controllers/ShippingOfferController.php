<?php

namespace App\Http\Controllers;

use App\Carrier;
use Illuminate\Http\Request;
use App\ShippingOffer;
use App\Shipping;

class ShippingOfferController extends Controller
{
    public function index($id)
    {
        $data = ShippingOffer::where('shipping_id', $id)->get();
        return view('backend.shipping_offer.index', compact('data', 'id'));
    }

    public function create()
    {
        $shipping = Shipping::findOrFail(\request('parent_id') * 1);
        $carriers = Carrier::all();
        return view('backend.shipping_offer.create', compact('shipping', 'carriers'));
    }

    public function edit($id)
    {
        $data = ShippingOffer::findOrFail($id);
        $shipping = $data->shipping;
        $carriers = Carrier::all();
        return view('backend.shipping_offer.edit', compact('data', 'shipping', 'carriers'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'price' => 'required',
            'size' => 'required',
            'weight' => 'required',
            'carrier_id' => 'required',
            'shipping_id' => 'required',
        ]);
        $offer = ShippingOffer::create($request->except('_token'));
        return redirect()->action('ShippingOfferController@index', $offer->id)->with('success', 'Успешно добавлено');
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'price' => 'required',
            'size' => 'required',
            'weight' => 'required',
            'carrier_id' => 'required',
        ]);
        $offer = ShippingOffer::findOrFail($id);
        $offer->update($request->except('_token', 'method'));
        return redirect()->action('ShippingOfferController@index', $offer->id)->with('success', 'Успешно');
    }
}
