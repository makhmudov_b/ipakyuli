<?php

namespace App\Http\Controllers;

use App\Vendor;
use Illuminate\Http\Request;
use App\Product;
use Auth;
use App\Country;
use App\City;
use App\Category;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    public function index($vendorAlias)
    {
//        $clean = \Artisan::call('optimize');
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        if (Auth::user()->hasRole('admin')) {
            $data = Product::where('vendor_id', $vendor->id)->with('size')->get();
        } else {
            if (Auth::user()->hasRole('manager')) {
                $data = Product::where('vendor_id', $vendor->id)->with('size')->get();
            } else {
                $data = null;
            }
        }
        return view('backend.products.index', compact('data', 'vendor'));
    }

    public function create($vendorAlias)
    {
        $country = Country::all();
        $city = City::all();
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $categories = Category::whereNull('parent_id')->with('sub_category', 'parent_category')->get();
        return view('backend.products.create', compact('country', 'city', 'vendor', 'categories'));
    }

    public function edit($vendorAlias, $alias)
    {
        $data = Product::where('alias', $alias)->firstOrFail();
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $directory = "uploads/product/" . $data->id;
        $images = \File::glob($directory . "/*.jpg");
        $categories = Category::whereNull('parent_id')->with('sub_category', 'parent_category')->get();
        return view('backend.products.edit', compact('data', 'images', 'vendor', 'categories'));
    }

    public function store(Request $request, $vendorAlias)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required',
            'category_id' => 'required',
        ]);
        $request['vendor_id'] = Vendor::where('alias', $vendorAlias)->firstOrFail()->id;
        $request['enabled'] = 1;
        $price = $request['pricing'];
        $amount = $request['amount'];
        $produce = $request['produce'];
        $price_amount = [];
        foreach ($price as $key => $item) {
            array_push($price_amount, [$amount[$key], $item, $produce[$key]]); /// [amount,price , produce-days]
        }
        $request['price'] = json_encode($price_amount);
        $product = Product::create($request->except('_token', 'image', 'pricing', 'amount', 'produce'));
        if ($request->file('image')) {
            $this->createFolder('product', $product->id);
            $images = $request->file('image');
            $this->storeImages($images, $product->id);
        }
        return redirect()->action('ProductController@index', $vendorAlias)->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $vendorAlias, $alias)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required',
            'category_id' => 'required',
        ]);
        $product = Product::where('alias', $alias)->firstOrFail();
        $price = $request['pricing'];
        $amount = $request['amount'];
        $produce = $request['produce'];
        $price_amount = [];
        foreach ($price as $key => $item) {
            array_push($price_amount, [$amount[$key], $item, $produce[$key]]); /// [amount, price , produce-days]
        }
        $request['price'] = json_encode($price_amount);
        $product->update($request->except('_token', 'image', 'pricing', 'amount', 'produce'));
        if ($request->file('image')) {
            $this->createFolder('product', $product->id);
            $images = $request->file('image');
            $this->storeImages($images, $product->id);
        }
        return redirect()->action('ProductController@index', $vendorAlias)->with('success', 'Успешно добавлено');
    }

    public function createFolder($directory, $productId)
    {
        $path = public_path('uploads/' . $directory . '/' . $productId);
        $directory_path = public_path('uploads/' . $directory);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($images, $productId)
    {
        foreach ($images as $key => $image) {
            $filename = $key + strtotime("now") . '.jpg';
            $path = public_path('uploads/product/' . $productId . '/' . $filename);
            Image::make($image->getRealPath())->encode('jpg', 60)
                ->fit(520, 520)
                ->save($path);
        }
        return 1;
    }

    public function removeImage($alias)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/product/' . $alias . '/' . $file_name);
        \File::delete($pathToDestroy);
        return response('okay', 200);
    }

    public function switch($vendorAlias, $alias)
    {
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $product = Product::where('alias', $alias)->firstOrFail();
        if ($vendor->id == $product->vendor_id) {
            $product->update([
                'enabled' => !$product->enabled
            ]);
        }
        return redirect()->action('DashboardController@index')->with('success', 'Изменения успешно внесены');
    }
    public function switchRecommend($vendorAlias, $alias)
    {
        $vendor = Vendor::where('alias', $vendorAlias)->firstOrFail();
        $product = Product::where('alias', $alias)->firstOrFail();
        if ($vendor->id == $product->vendor_id) {
            $product->update([
                'recommend' => !$product->recommend
            ]);
        }
        return redirect()->action('DashboardController@index')->with('success', 'Изменения успешно внесены');
    }

    public function removeProduct($vendorAlias, $alias)
    {
        $product = Product::where('alias', $alias)->with('size.type')->firstOrFail();
        Vendor::where('alias', $vendorAlias)->firstOrFail();
        if (count($product->size)) {
            if (count($product->size->type)) {
                $product->size()->type()->delete();
            }
            $product->size()->delete();
        }
        $product->delete();
        return redirect()->action('ProductController@index', $vendorAlias)->with('success', 'Успешно удален');
    }
}
