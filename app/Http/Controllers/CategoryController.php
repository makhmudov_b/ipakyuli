<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::whereNull('parent_id')->with('sub_category', 'parent_category')->get();
        $categories = Category::where('main', 1)->with('products')->take(4);
        return view('backend.category.index', compact('data', 'categories'));
    }

    public function create()
    {
        $data = Category::all();
        return view('backend.category.create', compact('data'));
    }

    public function toggleMain($id)
    {
        $data = Category::findOrFail($id);
        $data->update([
            'main' => !$data->main
        ]);
        return redirect()->back();
    }

    public function edit($id)
    {
        $data = Category::findOrFail($id);
        $categories = Category::where('id', '!=', $id)->with('sub_category')->get();
        $directory = "uploads/categories/".$id;
        $images = \File::glob($directory."/*.jpg");
        return view('backend.category.edit', compact('data', 'categories', 'images'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required'
        ]);
        if ($request['parent_id'] == 0) {
            $request['parent_id'] = null;
        }

        $category = Category::create($request->except('image', '_token'));
        if ($request->file('image')) {
            $this->createFolder('categories', $category->id);
            $images = $request->file('image');
            $this->storeImages($images, $category->id);
        }
        return redirect()->action('CategoryController@index')->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $alias)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required'
        ]);
        if ($request['parent_id'] == 0) {
            $request['parent_id'] = null;
        }
        $category = Category::where('alias', $alias)->firstOrFail();
        $category->update($request->except('image', '_token', '_method'));
        if ($request->file('image')) {
            $this->createFolder('categories', $category->id);
            $images = $request->file('image');
            $this->storeImages($images, $category->id);
        }
        return redirect()->action('CategoryController@index')->with('success', 'Успешно изменено');
    }

    public function createFolder($directory, $vendorId)
    {
        $directory_path = public_path('uploads/'.$directory);
        $path = public_path('uploads/'.$directory.'/'.$vendorId);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($images, $vendorId)
    {
        foreach ($images as $key => $image) {
            $filename = $key + strtotime("now").'.jpg';
            $path = public_path('uploads/categories/'.$vendorId.'/'.$filename);
            Image::make($image->getRealPath())->encode('jpg', 60)
                ->fit(310, 310)
                ->save($path);
        }
        return 1;
    }

    public function removeImage($id)
    {
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/categories/'.$id.'/'.$file_name);
        \File::delete($pathToDestroy);
        return response('okay', 200);
    }
}
