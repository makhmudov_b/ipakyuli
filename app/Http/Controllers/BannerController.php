<?php

namespace App\Http\Controllers;

use App\Vendor;
use Illuminate\Http\Request;
use App\Banner;
use Intervention\Image\ImageManagerStatic as Image;

class BannerController extends Controller
{
    public function index()
    {
        $data = Banner::all();
        return view('backend.banner.index',compact('data'));
    }
    public function create()
    {
        $vendor = Vendor::with('product')->get();
        return view('backend.banner.create', compact('vendor'));
    }
    public function edit($id)
    {
        $data = Banner::find($id);
        $vendor = Vendor::with('product')->get();
        $directory = "uploads/banner/".$id;
        $images = \File::glob($directory . "/*.jpg");
        return view('backend.banner.edit',compact('data','id','vendor','images'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_uz' => 'required',
            'image' => 'required'
        ]);
        $banner = Banner::create([
            'name_ru' => $request->name_ru,
            'name_uz' => $request->name_uz,
        ]);
        if($request->file('image')){
            $this->createFolder('banner',$banner->id);
            $images = $request->file('image');
            $this->storeImages($images,$banner->id);
        }
        return redirect()->action('BannerController@index')->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_uz' => 'required',
        ]);
        $banner = Banner::findOrFail($id);
        $banner->update($request->except('image','method'));
        if($request->file('image')){
            $this->createFolder('banner',$banner->id);
            $images = $request->file('image');
            $this->storeImages($images,$banner->id);
        }
        return redirect()->action('BannerController@index')->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $banner = Banner::findOrFail($id);
        $this->cleanDirectory($banner->id);
        $banner->delete();
        return redirect()->action('BannerController@index')->with('success','Успешно удален');
    }
    public function createFolder($directory,$bannerId)
    {
        $path = public_path('uploads/'.$directory.'/'.$bannerId);
        $directory_path = public_path('uploads/'.$directory);
        if(!\File::isDirectory($directory_path)){
            \File::makeDirectory($directory_path,0777,true,true);
        }
        if(!\File::isDirectory($path)){
            \File::makeDirectory($path,0777,true,true);
        }
        return 1;
    }
    public function storeImages($images,$bannerId)
    {
        foreach($images as $key => $image){
            $filename = $key .'.jpg';
            $path = public_path('uploads/banner/'.$bannerId.'/'. $filename);
            Image::make($image->getRealPath())->encode('jpg', 60)
            ->crop(1440, 420)
            ->save($path);
        }
        return 1;
    }
    public function removeImage($bannerId)
    {
        $file_name = $bannerId.'.jpg';
        $pathToDestroy = public_path('uploads/banner/'.$bannerId.'/'.$file_name);
        \File::delete($pathToDestroy);
        return response('okay',200);
    }
    public function cleanDirectory($id)
    {
        $pathToDestroy = public_path('uploads/banner/'.$id.'/');
        \File::cleanDirectory($pathToDestroy);
        return 1;
    }
}
