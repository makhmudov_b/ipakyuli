<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingOffer extends Model
{
    protected $guarded = [];

    public function shipping()
    {
        return $this->belongsTo('App\Shipping');
    }
    public function carrier()
    {
        return $this->belongsTo('App\Carrier');
    }
}
