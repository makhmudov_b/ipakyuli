<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $guarded = [];

    public function from()
    {
        return $this->belongsTo('App\City', 'from_id','id');
    }
    public function to()
    {
        return $this->belongsTo('App\City', 'to_id','id');
    }
    public function offer()
    {
        return $this->hasMany('App\ShippingOffer');
    }

}
