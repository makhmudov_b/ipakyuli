<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomelandDisease extends Model
{
    protected $guarded = [];

    public function homeland(){
        return $this->belongsTo('App\Homeland');
    }
}
