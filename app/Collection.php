<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $guarded = [];
    protected $appends = ['image', 'name', 'description'];

    public function products()
    {
        return $this->belongsToMany('App\Product', 'products_collection')->withTimestamps();
    }

    public function getImageAttribute()
    {
        $directory = "uploads/collection/" . $this->id;
        $images = \File::glob($directory . "/*.jpg");
        if (count($images) > 0) {
            return asset($images[0]);
        }
        return asset('img/collect.png');
    }

    public function getNameAttribute()
    {
        return $this->{'name_' . \App::getLocale()};
    }

    public function getDescriptionAttribute()
    {
        return $this->{'description_' . \App::getLocale()};
    }
}
