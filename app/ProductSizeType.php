<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class ProductSizeType extends Model
{
    use HasSlug;
    protected $guarded = [];
    protected $appends = ['name'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name_ru')
            ->saveSlugsTo('alias');
    }
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function size()
    {
        return $this->belongsTo('App\ProductSize','product_size_id');
    }
    public function getNameAttribute()
    {
    	return $this->{'name_'.\App::getLocale()};
    }
}
