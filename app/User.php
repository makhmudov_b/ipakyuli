<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','vendor_id','carrier_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * @param $role
     * @return mixed
     */
    public function hasRole($role)
    {
        return $this->roles()->where('type', $role)->first();
    }
    public function address()
    {
        return $this->hasMany('App\Address');
    }
    public function carrier()
    {
        return $this->belongsTo('App\Carrier');
    }
    public function vendor()
    {
        return $this->belongsTo('App\Vendor');
    }
    public function order()
    {
        return $this->hasMany('App\Order');
    }
}
