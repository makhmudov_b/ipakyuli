<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = [];
    protected $appends = ['image'];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function order()
    {
        return $this->belongsTo('App\Order','order_id');
    }
    public function getImageAttribute()
    {
        $directory = "uploads/message/".$this->id;
        $images = \File::glob($directory."/*.jpg");
        if (count($images) > 0) {
            return $images;
        }
        return [];
    }
}
