<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model
{
    use HasSlug;

    protected $guarded = [];
    /**
     * @var array
     */
    protected $appends = ['type', 'name', 'current', 'description', 'image'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name_ru')
            ->saveSlugsTo('alias');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Vendor', 'vendor_id');
    }

    public function discount()
    {
        return $this->hasMany('App\Discount');
    }

    public function size()
    {
        return $this->hasMany('App\ProductSize');
    }
    public function order_item()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function getNameAttribute()
    {
        return $this->{'name_'.\App::getLocale()};
    }

    public function getDescriptionAttribute()
    {
        return $this->{'description_'.\App::getLocale()};
    }

    public function getImageAttribute()
    {
        $directory = "uploads/product/".$this->id;
        $images = \File::glob($directory."/*.jpg");
        if (count($images) > 0) {
            return $images;
        }
        return [asset('img/no-photo.png')];
    }
    public function collection()
    {
        return $this->belongsToMany('App\Collection', 'products_collection');
    }
    public function getTypeAttribute()
    {
        if ($this->size->first()) {
            if ($this->size->first()->type->first()) {
                return 'structured';
            }
        }
        return 'default';
    }

    public function getCurrentAttribute()
    {
        $currency = ($this->currency == 'usd' ? $this->getDollarAttribute() : 1);
        if ($this->size->first()) {
            if ($this->size->first()->type->first()) {
                $price = json_decode($this->size->first()->type->first()->price, true)[0];
                return $price[1] / $price[0] * ($currency);
            }
        }
        $price = json_decode($this->price)[0];
        return $price[1] / $price[0] * ($currency);
    }

    public function getRangeAttribute()
    {
        $getItems = [];
        $currency = ($this->currency == 'usd' ? $this->getDollarAttribute() : 1);
        if ($this->size->first()) {
            if ($this->size->first()->type->first()) {
                foreach ($this->size->first()->type as $elem_type) {
                    $price = json_decode($elem_type->price, true);
                    foreach ($price as $ever_price) {
                        array_push($getItems, $ever_price[1]);
                    }
                }
                $max = max($getItems) * $currency;
                $min = min($getItems) * $currency;
                if ($max != $min) {
                    $range = [$min, $max];
                } else {
                    $range = [$min];
                }
                return $range;
            }
        }
        $price = json_decode($this->price);
        foreach ($price as $loop_price) {
            array_push($getItems, (int) $loop_price[1]);
        }
        $max = max($getItems) * $currency;
        $min = min($getItems) * $currency;
        if ($max != $min) {
            $range = [$min, $max];
        } else {
            $range = [$min];
        }
        return $range;
    }

    public function getDollarAttribute()
    {
        return Currency::first()->value;
    }
}
