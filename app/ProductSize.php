<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class ProductSize extends Model
{
    use HasSlug;

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('alias');
    }
    protected $appends = ['image'];
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'alias';
    }
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function type()
    {
        return $this->hasMany('App\ProductSizeType');
    }
    public function getImageAttribute()
    {
        $directory = "uploads/product_size/".$this->id;
        $images = \File::glob($directory . "/*.jpg");
        if(count($images) > 0)
        {
            return $images[0];
        }
        return 'img/Rectangle 29.png';
    }
}
