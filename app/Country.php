<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded = [];
    protected $appends = ['name'];

    public function city()
    {
        return $this->hasMany('App\City');
    }
    public function getNameAttribute()
    {
        return $this->{'name_'.\App::getLocale()};
    }
}
