<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Homeland extends Model
{
    protected $guarded = [];

    public function disease(){
        return $this->hasMany('App\HomelandDisease','homeland_id');
    }
}
