@extends('layouts.backend')

@section('content')
<section>
<div class="container">
<div class="row">
    <form action="{{ action('VendorController@update',$data->alias) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="white-block mb-30">
          <div class="head">
              <h3>Добавить прозводителя</h3>
          </div>
          <div class="content">
              <div class="input-block">
                <div class="input">
                    <label for="name">Название</label>
                    <input required="required" type="text" name="name" value="{{$data->name}}" class="form-control regStepOne" id="name" placeholder="" />
                </div>
              </div>
            <div class="input-block">
                <div class="input">
                    <label for="description_uz">Описание (UZ)</label>
                    <input required="required" type="text" name="description_uz" value="{{$data->description_uz}}" class="form-control regStepOne" id="description_uz" placeholder="" />
                </div>
              </div>
              <div class="input-block">
                <div class="input">
                    <label for="description_ru">Описание (RU)</label>
                    <input required="required" type="text" name="description_ru" value="{{$data->description_ru}}" class="form-control regStepOne" id="description_ru" placeholder="" />
                </div>
              </div>
              <div class="input-block">
                <div class="input">
                    <label>Страна:</label>
                    <select name="country_id" id="select-countries" class="form-control custom-select">
                        @foreach( $country as $datas )
                        <option data-data='{"image": "/backend/images/flags/{{$datas->flag}}.svg"}' value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                        @endforeach
                    </select>
                </div>
              </div>
              <div class="input-block">
                <div class="input">
                    <label>Город</label>
                    <select name="city_id" class="form-control" id="select-beast">
                        @foreach( $city as $datas )
                        <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                        @endforeach
                    </select>
                </div>
              </div>
                <div class="input-block">
                    <div class="input">
                        <label for="address">Адрес</label>
                        <input required="required" type="text" value="{{$data->address}}" class="regStepTwo" name="address" id="addressHotel" />
                    </div>
                    <div class="text-block max-992">
                        <p>укажите адрес, используя латинский алфавит</p>
                    </div>
                </div>
                  <div class="input-block">
                    <div class="input">
                        <label for="category">Выберите категорию</label>
                        <select multiple name="category_id[]" required="required" id="category" class="chosen-select form-control">
                            <optgroup label="Главные">
                            @foreach( $categories as $datas )
                                <option value="{{ $datas->id }}" @if(count($data->categories) > 0)
                                @if( in_array($datas->id, $data->categories->pluck('id')->toArray())) selected @endif
                                    @endif>
                                    {{ $datas->name_ru }}
                                </option>
                            @endforeach
                            </optgroup>
                            @foreach( $categories as $datas )
                                <optgroup label="{{$datas->name_ru}}">
                                    @foreach($datas->sub_category as $sub)
                                    <option value="{{ $sub->id }}" @if( in_array($sub->id, $data->categories->pluck('id')->toArray())) selected @endif>{{ $sub->name_ru }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                  </div>
{{--                @if(Auth::user()->hasRole('admin'))--}}
{{--                <div class="input-block">--}}
{{--                    <label for="manager">Менеджер</label>--}}
{{--                    <select name="user_id" class="form-control" id="manager">--}}
{{--                        @foreach( $user as $key => $datas )--}}
{{--                            @if($key != 0)--}}
{{--                        <option value="{{ $datas->id }}">--}}
{{--                            {{ $datas->name }}--}}
{{--                        </option>--}}
{{--                            @endif--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--                @endif--}}
                <div class="input-block">
                    <div class="input">
                        <label for="service-phone">Телефон отдела обслуживания </label>
                        <div class="phone">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone phone-icon"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>
                            <input required="required" type="tel" class="regStepTwo forPhone" name="phone" value="{{$data->phone}}" id="phoneHotel">
                        </div>
                    </div>
                    <div class="text-block max-992">
                        <p>с кодом страны, города или оператора</p>
                    </div>
                </div>
              <div class="input-block">
                <div class="input">
                    <label for="open_date">Дата открытия</label>
                    <input required="required" type="date" name="open_date" value="{{$data->open_date}}" class="form-control regStepOne" id="open_date" placeholder="" />
                </div>
              </div>
                <div class="input-block pb-3">
                    <div class="input">
                        <label>Локация:</label>
                        <div id="map" style="height: 400px; width: 100%;"></div>
                        <input id="long" value="69.240562" name="long" type="hidden" class="form-control">
                        <input id="lat" value="41.311081" name="lat" type="hidden" class="form-control">
                    </div>
                </div>
          </div>
        </div>
        <div class="white-block mb-30">
        <div class="head">
            <h3>Общие фотографии</h3>
        </div>
        <div class="card-header-after">
          <p class="pt-3">Добавьте общие фотографии прозводител, экстерьер и интерьер помещений. Мин. размер изображений 500x500 пикс.</p>
          <div class="uploader">
                <ul class="jFiler-items-list jFiler-items-grid">
                    @foreach($images as $image)
                    <li class="gallryUploadBlock_item photo-thumbler d-inline-block" data-image="{{basename($image)}}">
                        <div class="jFiler-item-thumb-image">
                            <img src="{{asset($image)}}" alt="image">
                        </div>
                    <div class="removeItem">
                        <span class="deletePhoto" data-image="{{basename($image)}}">
                            <i class="fe fe-minus"></i>
                        </span>
                    </div>
                    </li>
                    @endforeach
                </ul>
              <input type="file" name="image[]" class="filer_input3" multiple="multiple">
          </div>
        </div>
        </div>
        <div class="button-block">
            <button type="submit" class="continue-btn">Сохранить</button>
            <a href="{{ url()->current() }}" class="blue-text ml-40">Отменить изменения</a>
        </div>
    </form>
</div>
</div>
</section>
@endsection
@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&callback=initMap"></script>
<script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
<script>
$(document).ready(function(){
$(".filer_input3").filer({
  limit: null,
  maxSize: null,
  extensions: null,
  changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
  showThumbs: true,
  theme: "dragdropbox",
  templates: {
    box: '<ul class="jFiler-items-list buttonAdder jFiler-items-grid"></ul>',
    item: `<li class="gallryUploadBlock_item jFiler-item">
    {{'{{fi-image}'.'}'}}
    <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
    </li>`,
    progressBar: '<div class="bar"></div>',
    itemAppendToEnd: true,
    canvasImage: true,
    removeConfirmation: false,
    _selectors: {
      list: '.jFiler-items-list',
      item: '.jFiler-item',
      progressBar: '.bar',
      remove: '.fe.fe-minus'
    }
  },
  dragDrop: {
    dragEnter: null,
    dragLeave: null,
    drop: null,
    dragContainer: null,
  },
  files: null,
  addMore: true,
  allowDuplicates: false,
  clipBoardPaste: true,
  excludeName: null,
  beforeRender: null,
  afterRender: null,
  beforeShow: null,
  beforeSelect: null,
  itemAppendToEnd: true,
  onSelect: null,
  afterShow: function(jqEl, htmlEl, parentEl, itemEl){
    $('.galleryAddElement').hide()
    $('.cloneGalleryAddElement').remove()
    $('.buttonAdder').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
  },
  onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
    let filerKit = inputEl.prop("jFiler"),
          file_name = filerKit.files_list[id].name;
    //   $.post('./php/ajax_remove_file.php', {file: file_name});
      if(filerKit.files_list.length == 1){
        $('.galleryAddElement').show()
      }
  },
  onEmpty: null,
  options: null,
});
$('.uploader').on('click', '.cloneGalleryAddElement', function(e){
  $('.galleryAddElement').trigger('click');
});
$('.deletePhoto').click((e) =>{
    let removeImage = $(e.target);
    let imageData = removeImage.data('image');
    if(confirm('Вы уверены?')){
        let deleteItem = $.get("{{action('VendorController@removeImage',$data->alias)}}" , {file_name: imageData});
            deleteItem.done(() => {
                removeImage.parent().parent().remove();
            });
        }
});
let checkboxes = $('.checkboxes');
checkboxes.each(function() {
        let action = $(this).find('.action');
        let changer = ($(this).find('.changer'));
        changer.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
        });
        action.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
        });
});
});
var lattitude = $('#lat');
var longitude = $('#long');
function initMap() {
var latlng = new google.maps.LatLng({{$data->lat}}, {{$data->long}});
var map = new google.maps.Map(document.getElementById('map'), {
    center: latlng,
    zoom: 12,
        animation:google.maps.Animation.BOUNCE
});
var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    draggable: true
});
var lat ,long;
google.maps.event.addListener(marker, 'dragend', function (event) {
    lat  = this.getPosition().lat().toFixed(6);
    long = this.getPosition().lng().toFixed(6);
    lattitude.val(lat);
    longitude.val(long);
});
}
$(function() {
    $('.chosen-select').chosen();
    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
});
</script>
@endsection
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
@endsection
