@extends('layouts.backend')

@section('content')
<section>
<div class="container">
<div class="row">
    <form action="{{ action('CategoryController@update',$data->alias) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
        @csrf
        @method('PUT')
        <div class="white-block mb-30">
          <div class="head">
              <h3>Добавить Категорию</h3>
          </div>
          <div class="content">
              <div class="input-block">
                <div class="input">
                    <label for="name_ru">Название (RU)</label>
                    <input required="required" type="text" name="name_ru" value="{{ $data->name_ru }}" class="form-control regStepOne" id="name_ru" placeholder="" />
                </div>
              </div>
              <div class="input-block">
                <div class="input">
                    <label for="name_uz">Название (UZ)</label>
                    <input required="required" type="text" name="name_uz" value="{{ $data->name_uz }}" class="form-control regStepOne" id="name_uz" placeholder="" />
                </div>
              </div>
              @if($data->parent_id != null)
                <div class="input-block">
                    <div class="input">
                        <label>Привязанная категория:</label>
                        <select name="parent_id" class="form-control custom-select">
                            <option value="0">Отвязать от всех</option>
                            @foreach( $categories as $datas )
                                <option value="{{ $datas->id }}" @if($data->parent_id == $datas->id) selected @endif>{{ $datas->name_ru }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
              @endif
            <div class="input-block">
                <div class="input">
                    <label for="description_uz">Описание (UZ)</label>
                    <input required="required" type="text" name="description_uz" value="{{ $data->description_uz }}" class="form-control regStepOne" id="description_uz" placeholder="" />
                </div>
              </div>
              <div class="input-block">
                <div class="input">
                    <label for="description_ru">Описание (RU)</label>
                    <input required="required" type="text" name="description_ru" value="{{ $data->description_ru }}" class="form-control regStepOne" id="description_ru" placeholder="" />
                </div>
              </div>
          </div>
        </div>
        <div class="white-block mb-30">
        <div class="head">
            <h3>Общие фотографии</h3>
        </div>
        <div class="card-header-after">
          <p class="pt-3">Добавьте общие фотографии прозводител, экстерьер и интерьер помещений. Мин. размер изображений 500x500 пикс.</p>
          <div class="uploader">
                <ul class="jFiler-items-list jFiler-items-grid">
                    @foreach($images as $image)
                    <li class="gallryUploadBlock_item photo-thumbler d-inline-block" data-image="{{basename($image)}}">
                        <div class="jFiler-item-thumb-image">
                            <img src="{{asset($image)}}" alt="image">
                        </div>
                    <div class="removeItem">
                        <span class="deletePhoto" data-image="{{basename($image)}}">
                            <i class="fe fe-minus"></i>
                        </span>
                    </div>
                    </li>
                    @endforeach
                </ul>
              <input type="file" name="image[]" class="filer_input3" multiple="multiple">
          </div>
        </div>
        </div>
        <div class="button-block">
            <button type="submit" class="continue-btn">Сохранить</button>
        </div>
    </form>
</div>
</div>
</section>
@endsection

@section('script')
<script>
$(document).ready(function(){
$(".filer_input3").filer({
  limit: null,
  maxSize: null,
  extensions: null,
  changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
  showThumbs: true,
  theme: "dragdropbox",
  templates: {
    box: '<ul class="jFiler-items-list buttonAdder jFiler-items-grid"></ul>',
    item: `<li class="gallryUploadBlock_item jFiler-item">
    {{'{{fi-image}'.'}'}}
    <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
    </li>`,
    progressBar: '<div class="bar"></div>',
    itemAppendToEnd: true,
    canvasImage: true,
    removeConfirmation: false,
    _selectors: {
      list: '.jFiler-items-list',
      item: '.jFiler-item',
      progressBar: '.bar',
      remove: '.fe.fe-minus'
    }
  },
  dragDrop: {
    dragEnter: null,
    dragLeave: null,
    drop: null,
    dragContainer: null,
  },
  files: null,
  addMore: true,
  allowDuplicates: false,
  clipBoardPaste: true,
  excludeName: null,
  beforeRender: null,
  afterRender: null,
  beforeShow: null,
  beforeSelect: null,
  itemAppendToEnd: true,
  onSelect: null,
  afterShow: function(jqEl, htmlEl, parentEl, itemEl){
    $('.galleryAddElement').hide()
    $('.cloneGalleryAddElement').remove()
    $('.buttonAdder').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
  },
  onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
    let filerKit = inputEl.prop("jFiler"),
          file_name = filerKit.files_list[id].name;
      if(filerKit.files_list.length == 1){
        $('.galleryAddElement').show()
      }
  },
  onEmpty: null,
  options: null,
});
$('.uploader').on('click', '.cloneGalleryAddElement', function(e){
  $('.galleryAddElement').trigger('click');
});
$('.deletePhoto').click((e) =>{
    let removeImage = $(e.target);
    let imageData = removeImage.data('image');
    if(confirm('Вы уверены?')) {
        let deleteItem = $.get("{{action('CategoryController@removeImage',$data->id)}}", {file_name: imageData});
        deleteItem.done(() => {
            removeImage.parent().parent().remove();
        });
    }
});
let checkboxes = $('.checkboxes');
checkboxes.each(function() {
        let action = $(this).find('.action');
        let changer = ($(this).find('.changer'));
        changer.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
        });
        action.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
        });
});
});
</script>
@endsection
