@extends('layouts.backend')

@section('content')
<section>
<div class="container">
<div class="row">
    <form action="{{ action('CategoryController@store') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
        @csrf
        <div class="white-block mb-30">
          <div class="head">
              <h3>Добавить Категорию</h3>
          </div>
          <div class="content">
              <div class="input-block">
                <div class="input">
                    <label for="name_ru">Название (RU)</label>
                    <input required="required" type="text" name="name_ru" value="{{old('name_ru')}}" class="form-control regStepOne" id="name_ru" placeholder="" />
                </div>
              </div>
              <div class="input-block">
                <div class="input">
                    <label for="name_uz">Название (UZ)</label>
                    <input required="required" type="text" name="name_uz" value="{{old('name_uz')}}" class="form-control regStepOne" id="name_uz" placeholder="" />
                </div>
              </div>
            <div class="input-block">
                <div class="input">
                    <label>Привязанная категория:</label>
                    <select name="parent_id" class="form-control custom-select">
                        <option value="0">Не выбран</option>
                        @foreach( $data as $datas )
                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="input-block">
                <div class="input">
                    <label for="description_uz">Описание (UZ)</label>
                    <input required="required" type="text" name="description_uz" value="{{old('description_uz')}}" class="form-control regStepOne" id="description_uz" placeholder="" />
                </div>
              </div>
              <div class="input-block">
                <div class="input">
                    <label for="description_ru">Описание (RU)</label>
                    <input required="required" type="text" name="description_ru" value="{{old('description_ru')}}" class="form-control regStepOne" id="description_ru" placeholder="" />
                </div>
              </div>
          </div>
        </div>
        <div class="white-block mb-30">
        <div class="head">
            <h3>Общие фотографии</h3>
        </div>
        <div class="card-header-after">
          <p class="pt-3">Добавьте общие фотографии прозводител, экстерьер и интерьер помещений. Мин. размер изображений 500x500 пикс.</p>
          <div class="fotoUploader">
              <input type="file" name="image[]" class="filer_input2" multiple="multiple">
          </div>
        </div>
        </div>
        <div class="button-block">
            <button type="submit" class="continue-btn">Сохранить</button>
        </div>
    </form>
</div>
</div>
</section>
@endsection
