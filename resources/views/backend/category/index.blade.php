@extends('layouts.backend')

@section('content')
	<section>
		<div class="container">
			<h2 class="blue-title">Категории</h2>
			<div class="row">
				<form action="#" autocomplete="off">
					<div class="white-block">
                        <ul class="list-block">
                        @foreach ($data as $category)
                          <li class="list-group-item">
                            <div class="d-flex justify-content-between align-items-center">
                                <span>
                                    <i class="fe fe-activity"></i> {{ $category->name_ru }}
                                </span>
								<div class="right-block">
                                    <a href="{{ action('CategoryController@toggleMain',$category->id)  }}" class="btn btn-outline-success">
                                        @if($category->main) На главной <i class="fe fe-check"></i> @else Добавить на главную @endif
                                    </a>
									<a href="{{ action('CategoryController@edit',$category->id)  }}" class="btn btn-azure"><i class="fe fe-edit"></i></a>
								</div>
                            </div>

                            @if ($category->sub_category )
                              <ul class="list-group mt-3">
                                @foreach ($category->sub_category as $child)
                                  <li class="list-group-item">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span>*{{ $child->name_ru }}</span>
                                        <div class="right-block">
                                            <a href="{{ action('CategoryController@toggleMain',$child->id)  }}" class="btn btn-outline-success">
                                                @if($child->main) На главной <i class="fe fe-check"></i> @else Добавить на главную @endif
                                            </a>
                                            <a href="{{ action('CategoryController@edit',$child->id)  }}" class="btn btn-azure"><i class="fe fe-edit"></i></a>
                                        </div>
                                    </div>
                                      @if($child->sub_category)
                                          <ul class="list-group mt-3">
                                            @foreach ($child->sub_category as $child_inner)
                                              <li class="list-group-item">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span>*{{ $child_inner->name_ru }}</span>
                                                    <div class="right-block">
                                                        <a href="{{ action('CategoryController@toggleMain',$child_inner->id)  }}" class="btn btn-outline-success">
                                                            @if($child_inner->main) На главной <i class="fe fe-check"></i> @else Добавить на главную @endif
                                                        </a>
                                                        <a href="{{ action('CategoryController@edit',$child_inner->id)  }}" class="btn btn-azure"><i class="fe fe-edit"></i></a>
                                                    </div>
                                                </div>
                                              </li>
                                            @endforeach
                                          </ul>
                                      @endif
                                  </li>
                                @endforeach
                              </ul>
                            @endif
                          </li>
                        @endforeach
                        <a class="add-list-btn" href="{{ action('CategoryController@create') }}">
                            <i class="fe fe-plus-circle"></i>
                            Добавить категорию
                        </a>
                      </ul>
					</div>
					<div class="button-block">
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection
