@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title d-flex align-items-center"><a
                    href="{{action('VendorController@index',$vendor->alias)}}"
                    class="badge badge-primary mr-2">{{$vendor->name}}</a> <span>Продукты</span></h2>
            <div class="row">
                <form action="#" autocomplete="off">
                    <div class="white-block hotels">
                        <ul class="list-block">
                            @foreach($data as $product)
                                <li class="item">
                                    <a class="left-block"
                                       href="{{ action('ProductSizeController@index', [$vendor->alias , $product->alias]) }}">
                                        <i class="fe fe-align-center"></i>
                                        <p class="pl-2 d-flex align-items-center"><span>{{ $product->name_ru }}</span>
                                            <span
                                                class="ml-2 badge badge-dark">{{$product->type == 'default' ? 'Обычный' :'Типизированный'}}</span>
                                        </p>
                                    </a>
                                    <div class="right-block">
                                        <a href="{{ action('ProductSizeController@index', [$vendor->alias , $product->alias]) }}"
                                           class="btn btn-primary mr-5">Тип</a>
                                        <span class="icon">
										<label class="custom-switch">
                                        <span>Вкл/Выкл</span>
										  <input type="checkbox" name="custom-switch-checkbox"
                                                 class="custom-switch-input" @if($product->enabled) checked="checked" @endif>
										  <span class="custom-switch-indicator actioner"
                                                data-href="{{ action('ProductController@switch',[$vendor->alias , $product->alias]) }}"></span>
										  <span class="custom-switch-description"></span>
										</label>
									</span>
                                        <span class="icon">
										<label class="custom-switch">
                                        <span>Рекомменд</span>
										  <input type="checkbox" name="custom-switch-checkbox"
                                                 class="custom-switch-input" @if($product->recommend) checked="checked" @endif>
										  <span class="custom-switch-indicator action-maker"
                                                data-href="{{ action('ProductController@switchRecommend',[$vendor->alias , $product->alias]) }}"></span>
										  <span class="custom-switch-description"></span>
										</label>
									</span>
                                        <a href="{{ action('ProductController@edit',[$vendor->alias,$product->alias])  }}"
                                           class="edit icon"><i class="fe fe-edit"></i></a>
                                        <a href="{{ action('ProductController@removeProduct',[$vendor->alias,$product->alias])  }}"
                                           onclick="return confirm('Вы уверены?')" class="trash icon"><i
                                                class="fe fe-trash-2"></i></a>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ action('ProductController@create',$vendor->alias) }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить продукт
                            </a>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $('.actioner').on('click', function () {
            let url = $(this).data('href');
            $.get(url);
        })
        $('.action-maker').on('click', function () {
            let url = $(this).data('href');
            $.get(url);
        })
    </script>
@endsection
