@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('ProductController@store',$vendor->alias) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить продукт</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{old('name_ru')}}"
                                           class="form-control regStepOne" id="name_ru"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{ old('name_uz') }}"
                                           class="form-control regStepOne" id="name_uz"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <textarea name="description_ru" class="form-control regStepOne text-area"
                                              id="description_ru" cols="30"
                                              rows="10">{{old('description_ru')}}</textarea>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <textarea name="description_uz" class="form-control regStepOne text-area"
                                              id="description_uz" cols="30"
                                              rows="10">{{old('description_uz')}}</textarea>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите категорию</label>
                                    <select name="category_id" id="category" class="chosen-select form-control">
                                        <optgroup label="Главные">
                                            @foreach( $categories as $datas )
                                                <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                            @endforeach
                                        </optgroup>
                                        @foreach( $categories as $datas )
                                            <optgroup label="{{$datas->name_ru}}">
                                                @foreach($datas->sub_category as $sub)
                                                    <option value="{{ $sub->id }}">{{ $sub->name_ru }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30 changable" id="badsBlock">
                        <div class="head">
                            <h3>Цены</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="price_dropper">Скидка</label>
                                    <input required="required" type="text" name="price_dropper"
                                           value="{{old('price_dropper') ?? 0}}" class="form-control regStepOne"
                                           id="price_dropper"/>
                                </div>
                                <div class="input">
                                    <label for="sizes">Размер (м3)</label>
                                    <input required="required" type="text" name="sizes" value="{{old('sizes')}}"
                                           class="form-control regStepOne" id="sizes" placeholder=""/>
                                </div>
                                <div class="input">
                                    <label for="weight">Вес (кг)</label>
                                    <input required="required" type="text" name="weight" value="{{old('weight')}}"
                                           class="form-control regStepOne" id="weight" placeholder=""/>
                                </div>
                                <div class="input">
                                    <label for="min_count">Минимальное кол-во</label>
                                    <input required="required" type="number" name="min_count"
                                           value="{{old('min_count') ?? 0}}" class="form-control regStepOne"
                                           id="min_count"/>
                                </div>
                                <div class="input">
                                    <label for="count_type">Тип измерения (шт\кг\комплект)</label>
                                    <input required="required" type="text" name="count_type"
                                           value="{{old('count_type') ?? 0}}" class="form-control regStepOne"
                                           id="count_type"/>
                                </div>
                            </div>
                            <div class="input-block with-trash">
                                <div class="input">
                                    <div class="half-block max-992 mobile-mb-30">
                                        <p>Количество</p>
                                        <input required="required" type="number" value="1" name="amount[]"
                                               class="form-control" min="1"/>
                                    </div>
                                    <div class="half-block max-992">
                                        <p class="second">Цена</p>
                                        <input required="required" type="number" value="1" name="pricing[]"
                                               class="form-control" min="0"/>
                                    </div>
                                    <div class="half-block max-992">
                                        <p class="second">Кол-во дней на производство</p>
                                        <input required="required" type="number" value="1" name="produce[]"
                                               class="form-control" min="1"/>
                                    </div>
                                </div>
                            </div>
                            <div class="input-block with-trash text-block">
                                <span class="add-button d-flex align-items-center" style="height: 42px;"
                                      id="btnAddprice"><i
                                        class="fe fe-plus-circle"></i> <span>Добавить цену</span></span>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="currency">Выберите валюту</label>
                                    <select name="currency" id="currency" class="chosen-select form-control">
                                        <option value="usd">USD</option>
                                        <option value="uzs">UZS</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Общие фотографии</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте общие фотографии прозводител, экстерьер и интерьер помещений. Мин.
                                размер изображений 500x500 пикс.</p>
                            <div class="fotoUploader">
                                <input required="required" type="file" name="image[]" class="filer_input2"
                                       multiple="multiple">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script
        src="https://cdn.tiny.cloud/1/qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc/tinymce/5/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea.text-area',
            height: 500,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            content_css: [
                '{{ asset('css/styles.css') }}'
            ]
        });

        let btnAddprice = $('#btnAddprice');
        let btnRemovePrice = $('.btnRemovePrice');
        let priceHtml = `<div class="input-block with-trash">
                    <div class="input">
                        <div class="half-block max-992 mobile-mb-30">
                            <p>Количество</p>
                            <div class="d-inline-block selectize-control">
                                <input required="required" type="number" value="1" name="amount[]" class="form-control" min="1" />
                            </div>
                        </div>
                        <div class="half-block max-992">
                            <p>Цена</p>
                            <input required=required" type="number" name="pricing[]" min="1" value="1">
                        </div>
                        <div class="half-block max-992">
                            <p class="second">Кол-во дней на производство</p>
                            <input required="required" type="number" value="1" name="produce[]" class="form-control" min="1" />
                        </div>
                    </div>
                    <div class="text-block">
                        <a href="#" class="btnRemovePrice"><i class="fa-lg fe fe-trash-2"></i></a>
                    </div>
                    </div>`;
        btnAddprice.on('click', function () {
            let count = $('.with-trash').length;
            if (count <= 10) {
                $(this).parent().before(priceHtml)
            } else {
                alert('Лимит цены привышен');
            }
        });
        $('#badsBlock').on('click', '.btnRemovePrice', function (e) {
            e.preventDefault();
            $(this).closest('.with-trash').remove();
        })
    </script>
@endsection
