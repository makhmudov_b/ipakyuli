@extends('layouts.backend')

@section('content')
<section>
    <div class="container mb-5">
        <h2 class="blue-title">Регион</h2>
        <div class="row">
            <form action="#" autocomplete="off">
                <div class="white-block hotels">
                    <ul class="list-block">
                        @foreach($data as $datas)
                        <li class="item">
                            <div class="left-block">
                                <p class="pl-2">{{ $datas->name_ru  }}</p>
                            </div>
                            <div class="right-block">
                                <a href="{{ action('CityController@edit' , $datas->id) }}" class="btn btn-primary">
                                    <i class="fe fe-edit"></i>
                                </a>
                            </div>
                        </li>
                        @endforeach
                        <a class="add-list-btn" href="{{ action('CityController@create',$id) }}">
                            <i class="fe fe-plus-circle"></i>
                            Добавить
                        </a>
                    </ul>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
