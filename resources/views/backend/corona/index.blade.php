@extends('layouts.backend')

@section('content')
	<section>
		<div class="container">
            <div class="d-flex align-items-center justify-content-between">
                <h2 class="blue-title">Corona</h2>
                <div>
                <a href="{{ action('SettingController@refreshCorona')  }}" class="btn btn-primary"><i class="fe fe-refresh-ccw"></i></a>
                <a href="{{ action('SettingController@optimize')  }}" class="btn btn-primary">Сбросить кэш</a>
                </div>
            </div>
			<div class="row">
				<form action="#" autocomplete="off">
					<div class="white-block hotels">
						<ul class="list-block">
                            @foreach($data as $corona)
							<li class="item">
								<div class="left-block">
									<i class="fe fe-home"></i>
									<p class="pl-2">{{ $corona->title  }}</p>
								</div>
								<div class="right-block">
									<a href="{{ action('SettingController@edit',$corona->id)  }}" class="edit icon"><i class="fe fe-edit"></i></a>
								</div>
							</li>
                            @endforeach
							<a class="add-list-btn" href="{{ action('SettingController@create') }}">
								<i class="fe fe-plus-circle"></i>
								Добавить статью
							</a>
						</ul>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection
