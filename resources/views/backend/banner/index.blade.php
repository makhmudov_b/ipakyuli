@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Баннеры</h2>
            <div class="row">
                <div class="form">
                    <div class="white-block hotels">
                        <ul class="list-block">
                            @foreach($data as $datas)
                                <li class="item">
                                    <a class="left-block" href="{{ action('BannerController@edit' , $datas->id) }}">
                                        <p class="pl-2">{{ $datas->name_ru  }}</p>
                                    </a>
                                    <div class="right-block">
                                        <a href="{{ action('BannerController@edit' , $datas->id) }}"
                                           class="btn btn-primary">
                                            <i class="fe fe-edit"></i>
                                        </a>
                                        <form class="d-inline-block"
                                              action="{{ action('BannerController@delete' , $datas->id) }}"
                                              method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn icon border-0"
                                                    onclick="return confirm('Вы уверены?')">
                                                <i class="fe fe-trash"></i>
                                            </button>
                                        </form>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ action('BannerController@create') }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
