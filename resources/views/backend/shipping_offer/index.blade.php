@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Предложения</h2>
            <div class="row">
                <form action="{{ action('ShippingOfferController@create') }}" autocomplete="off">
                    <input type="hidden" name="parent_id" value="{{$id}}">
                    <div class="white-block hotels">
                        <ul class="list-block">
                            @foreach($data as $carrier)
                                <li class="item">
                                    <a class="left-block"
                                       href="{{ action('ShippingOfferController@edit',$carrier->id) }}">
                                        <i class="fe fe-map-pin"></i>
                                        <p class="pl-2">{{ $carrier->name  }}</p>
                                    </a>
                                    <div class="right-block">
                                        <a href="{{ action('ShippingOfferController@edit',$carrier->id)  }}"
                                           class="edit icon"><i class="fe fe-edit"></i></a>
                                    </div>
                                </li>
                            @endforeach
                            <button type="submit" class="add-list-btn">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </button>
                        </ul>
                    </div>
                    <div class="button-block">
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $('.action-maker').on('click', function () {
            let url = $(this).data('href');
            $.get(url);
        })
    </script>
@endsection
