@extends('layouts.backend')

@section('content')
    <div class="container">
        @if(!count($messages))
            <div class="row justify-content-center align-items-center">
                <h2>Пока нет сообщений</h2>
            </div>
        @endif
        @if($order->status == 0 && count($messages))
            <div class="row justify-content-end align-items-center">
                <div style="margin-right: 10px;" class="badge badge-primary">Нынешняя цена : {{$order->price}}</div>
                <form action="{{action('OrderController@changePrice',$order->id)}}">
                    <div class="input-group">
                        <input type="number" name="price" value="{{$order->price}}" class="form-control">
                        <button class="btn btn-primary">Изменить цену</button>
                    </div>
                </form>
            </div>
        @endif

        @foreach($messages as $message)
            <div class="row row-cards row-deck">
                @if($message->receiver_id != Auth::user()->vendor_id)
                    <div class="col-lg-6"></div>
                @endif
                <div class="col-lg-6">
                    <div class="card card-aside">
                        <div class="card-aside-column"
                             style="background-size: contain ; @if(count($message->image)) background-image: url({{asset($message->image[0])}});  @endif background-color: rgba(0,0,0,.5);"></div>
                        <div class="card-body d-flex flex-column">
                            <h4>{{$message->body}}</h4>
{{--                            <div class="text-muted">{{$order->order_item[0]->name}}</div>--}}
                            <div class="d-flex align-items-center pt-5 mt-auto">
                                <div>
                                    <div class="text-default">От: {{$message->user->name}}</div>
                                    <small class="d-block text-muted">{{$message->created_at}}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        @if(count($messages))
            <form enctype="multipart/form-data" method="POST" action="{{action('MessageController@send')}}"
                  class="form-group">
                @csrf
                <div class="input-group">
                <span class="col-auto">
                    <input type="file" name="image" class="form-control-file">
                </span>
                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <input type="hidden" name="receiver_id" value="{{$messages[0]->user_id}}">
                    <input type="text" class="form-control" name="body" placeholder="Напишите сообщение..."
                           required="required">
                    <span class="input-group-append">
                  <button class="btn btn-primary">Отправить</button>
                </span>
                </div>
            </form>
        @endif
    </div>
@endsection
