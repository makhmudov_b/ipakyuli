@extends('layouts.backend')

@section('content')
<section>
		<div class="container">
			<div class="row">
				<form action="{{ action('UserController@update') }}" method="POST" autocomplete="off">
                    @csrf
                    @method('PUT')
					<div class="white-block">
						<div class="head">
							<h3><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>Личные данные</h3>
						</div>
						<div class="content mb-30 mobile-mb-5">
							<div class="input-block">
								<div class="input">
									<label for="name">Имя</label>
									<input type="text" required="required" name="name" id="name" value="{{$user->name}}">
								</div>

							</div>
							<div class="input-block">
								<div class="input">
									<label for="email">E-mail</label>
									<input type="email" required="required" name="email" id="email" value="{{$user->email}}">
								</div>

							</div>
							<div class="input-block">
								<div class="input">
									<label for="phone">Телефон для уведомлений</label>
									<div class="phone">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone phone-icon"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>
										<input type="tel" required="required" name="phone" id="phone" value="{{$user->phone}}">
									</div>
								</div>
								<div class="text-block">
									<p>На указанный номер вы будете получать СМС-уведомления о бронированиях с сайта</p>
								</div>
							</div>
						</div>
					</div>
					<div class="white-block">
						<div class="head">
							<h3><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>Смена пароля</h3>
						</div>
						<div class="content mb-30">
							<div class="input-block">
								<div class="input">
									<label for="newPassword">Укажите старый пароль:</label>
									<input type="password" name="current-password" id="password" />
								</div>
								<div class="text-block">
									<p>Если вы не желаете менять пароль, оставьте поля пустыми</p>
								</div>
							</div>
							<div class="input-block">
								<div class="input">
									<label for="newPassword">Новый пароль:</label>
									<input type="password" name="new-password" id="newPassword" />
								</div>

							</div>
							<div class="input-block">
								<div class="input">
									<label for="confirmNewPassword">Еще раз, новый пароль:</label>
									<input type="password" name="new-password_confirmation" id="confirmNewPassword" />
								</div>
							</div>
						</div>
					</div>
					<div class="button-block">
						<button type="submit" class="continue-btn">Сохранить</button>
						<a href="#" class="blue-text ml-40">Отменить изменения</a>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection
