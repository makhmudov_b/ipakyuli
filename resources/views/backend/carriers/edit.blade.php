@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('CarrierController@update',$data->id) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить прозводителя</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" type="text" name="name" value="{{$data->name}}"
                                           class="form-control regStepOne" id="name" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <input required="required" type="text" name="description_uz"
                                           value="{{$data->description_uz}}" class="form-control regStepOne"
                                           id="description_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <input required="required" type="text" name="description_ru"
                                           value="{{$data->description_ru}}" class="form-control regStepOne"
                                           id="description_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Страна:</label>
                                    <select name="country_id" id="select-countries" class="form-control custom-select">
                                        @foreach( $country as $datas )
                                            <option data-data='{"image": "/backend/images/flags/{{$datas->flag}}.svg"}'
                                                    value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Город</label>
                                    <select name="city_id" class="form-control" id="select-beast">
                                        @foreach( $city as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="address">Адрес</label>
                                    <input required="required" type="text" value="{{$data->address}}" class="regStepTwo"
                                           name="address" id="addressHotel"/>
                                </div>
                                <div class="text-block max-992">
                                    <p>укажите адрес, используя латинский алфавит</p>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="service-phone">Телефон отдела обслуживания </label>
                                    <div class="phone">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-phone phone-icon">
                                            <path
                                                d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                        </svg>
                                        <input required="required" type="tel" class="regStepTwo forPhone" name="phone"
                                               value="{{$data->phone}}" id="phoneHotel">
                                    </div>
                                </div>
                                <div class="text-block max-992">
                                    <p>с кодом страны, города или оператора</p>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="open_date">Дата открытия</label>
                                    <input required="required" type="date" name="open_date" value="{{$data->open_date}}"
                                           class="form-control regStepOne" id="open_date" placeholder=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                        <a href="{{ url()->current() }}" class="blue-text ml-40">Отменить изменения</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
