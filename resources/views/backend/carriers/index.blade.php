@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Доставшики</h2>
            <div class="row">
                <form action="#" autocomplete="off">
                    <div class="white-block hotels">
                        <ul class="list-block">
                            @foreach($data as $carrier)
                                <li class="item">
                                    <a class="left-block" href="{{ action('CarrierController@show',$carrier->id) }}">
                                        <i class="fe fe-map-pin"></i>
                                        <p class="pl-2">{{ $carrier->name  }}</p>
                                    </a>
                                    <div class="right-block">
                                        <a href="{{ action('CarrierController@edit',$carrier->id)  }}"
                                           class="edit icon"><i class="fe fe-edit"></i></a>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ action('CarrierController@create') }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </a>
                        </ul>
                    </div>
                    <div class="button-block">
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $('.action-maker').on('click', function () {
            let url = $(this).data('href');
            $.get(url);
        })
    </script>
@endsection
