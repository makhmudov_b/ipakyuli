@extends('layouts.backend')

@section('content')
	<section>
		<div class="container">
            <div class="d-flex align-items-center justify-content-between">
                <h2 class="blue-title">Валюта</h2>
                <div>
                <a href="{{ action('CurrencyController@update')  }}" class="btn btn-primary"><i class="fe fe-refresh-ccw"></i></a>
                </div>
            </div>
			<div class="row">
				<form action="#" autocomplete="off">
					<div class="white-block hotels">
						<ul class="list-block">
                            @foreach($data as $corona)
							<li class="item">
								<div class="left-block">
									<i class="fe fe-dollar-sign"></i>
									<p class="pl-2">Цена доллара : {{ $corona->value  }} UZS</p>
								</div>
							</li>
                            @endforeach
						</ul>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection
