@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('CollectionController@store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{$data->name_ru}}"
                                           class="form-control regStepOne" id="name_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{$data->name_uz}}"
                                           class="form-control regStepOne" id="name_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <input required="required" type="text" name="description_ru"
                                           value="{{$data->description_ru}}"
                                           class="form-control regStepOne" id="description_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <input required="required" type="text" name="description_uz"
                                           value="{{$data->description_uz}}"
                                           class="form-control regStepOne" id="description_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите продукты</label>
                                    <select multiple name="product_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $products as $datas )
                                            <option
                                                value="{{ $datas->id }}"
                                                @if( in_array($datas->id, $data->products->pluck('id')->toArray())) selected @endif>{{ $datas->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Фото:</label>
                                    <input type="file" name="image[]" class="image">
                                    <p><h4>Загрузите для обновления фотографии</h4></p>
                                    <div class="d-flex justify-content-between mt-5">
                                        @foreach($images as $image)
                                            <img src="{{asset($image)}}?time={{microtime(true)}}" alt="image"
                                                 width="150"/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
    <script>
        $(function () {
            $('#category').chosen();
        });
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('backend/css/chosen.css')}}">
@endsection
