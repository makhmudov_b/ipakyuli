@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('CollectionController@store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Добавить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{old('name_ru')}}"
                                           class="form-control regStepOne" id="name_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{old('name_uz')}}"
                                           class="form-control regStepOne" id="name_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_ru">Описание (RU)</label>
                                    <input required="required" type="text" name="description_ru"
                                           value="{{old('description_ru')}}"
                                           class="form-control regStepOne" id="description_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description_uz">Описание (UZ)</label>
                                    <input required="required" type="text" name="description_uz"
                                           value="{{old('description_uz')}}"
                                           class="form-control regStepOne" id="description_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="category">Выберите продукты</label>
                                    <select multiple name="product_id[]" required id="category"
                                            class="chosen-select form-control">
                                        @foreach( $products as $datas )
                                            <option value="{{ $datas->id }}">{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>Фото: 370x300</label>
                                    <input type="file" required="required" name="image[]">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
    <script>
        $(function () {
            $('#category').chosen();
        });
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('backend/css/chosen.css')}}">
@endsection
