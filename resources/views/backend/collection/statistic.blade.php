@extends('layouts.backend')

@section('content')
    <br><br>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-12">
                <div class="row">
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$vendors->count()}}</div>
                                <h3 class="mb-1">Кол-во производителей</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$products->count()}}</div>
                                <h3 class="mb-1">Кол-во продуктов</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-value float-right text-blue">{{$users->count()}}</div>
                                <h3 class="mb-1">Кол-во юзеров</h3>
                                <div class="text-muted">Активных</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
