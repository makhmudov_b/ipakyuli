@extends('layouts.backend')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <form action="{{ action('CountryController@update',$data->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="white-block mb-30">
                  <div class="head">
                      <h3>Изменить</h3>
                  </div>
                  <div class="content">
                    <div class="input-block">
                        <div class="input">
                            <label for="name_ru">Название (RU)</label>
                            <input required="required" type="text" name="name_ru" value="{{$data->name_ru}}" class="form-control regStepOne" id="name_ru" />
                        </div>
                    </div>
                    <div class="input-block">
                        <div class="input">
                            <label for="name_uz">Название (UZ)</label>
                            <input required="required" type="text" name="name_uz" value="{{$data->name_uz}}" class="form-control regStepOne" id="name_ru" />
                        </div>
                    </div>
                  </div>
                </div>
                <div class="button-block">
                    <button type="submit" class="continue-btn">Добавить</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
