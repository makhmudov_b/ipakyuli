@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Доставка</h2>
            <div class="row">
                <form action="#" autocomplete="off">
                    <div class="white-block hotels">
                        <ul class="list-block">
                            @foreach($data as $carrier)
                                <li class="item">
                                    <a class="left-block" href="{{ action('ShippingController@edit',$carrier->id) }}">
                                        <i class="fe fe-map-pin"></i>
                                        <p class="pl-2">{{ $carrier->from->name_ru .' - '.$carrier->to->name_ru  }}</p>
                                    </a>
                                    <div class="right-block">
                                        <a href="{{ action('ShippingController@edit',$carrier->id)  }}"
                                           class="edit icon"><i class="fe fe-edit"></i></a>
                                        <a href="{{action('ShippingOfferController@index',$carrier->id)}}" class="btn btn-primary"><i
                                                class="fe fe-arrow-right"></i></a>
                                    </div>
                                </li>
                            @endforeach
                            <a class="add-list-btn" href="{{ action('ShippingController@create') }}">
                                <i class="fe fe-plus-circle"></i>
                                Добавить
                            </a>
                        </ul>
                    </div>
                    <div class="button-block">
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $('.action-maker').on('click', function () {
            let url = $(this).data('href');
            $.get(url);
        })
    </script>
@endsection
