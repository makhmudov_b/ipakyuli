@extends('layouts.backend')

@section('content')
	<section>
		<div class="container">
			<h2 class="blue-title d-flex align-items-center">
                <a href="{{action('VendorController@index',[$vendor->alias])}}" class="badge badge-primary mr-2">{{$vendor->name}}</a>
                <a href="{{action('ProductController@index',[$vendor->alias,$product->alias])}}">{{$product->name_ru}}</a>
                <a href="{{action('ProductSizeController@index',[$vendor->alias,$product->alias,$product_size->id])}}" class="btn btn-azure ml-2">{{$product_size->name}}</a>
            </h2>
			<div class="row">
				<form action="#" autocomplete="off">
					<div class="white-block hotels">
						<ul class="list-block">
                            @foreach($data as $product_size_type)
							<li class="item">
								<a class="left-block" href="{{ action('ProductSizeTypeController@edit', [$vendor->alias , $product->alias , $product_size->id , $product_size_type->id]) }}">
									<i class="fe fe-zap"></i>
									<p class="pl-2">{{ $product_size_type->name_ru  }}</p>
								</a>
								<div class="right-block">
									<span class="icon">
										<label class="custom-switch">
										  <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" @if($product_size_type->available) checked="checked" @endif>
										  <span class="custom-switch-indicator actioner" data-href="{{ action('ProductSizeTypeController@switch',$product_size_type->id) }}"></span>
										  <span class="custom-switch-description"></span>
										</label>
									</span>
									<a href="{{ action('ProductSizeTypeController@edit',[$vendor->alias , $product->alias , $product_size->id, $product_size_type->id])  }}" class="edit icon"><i class="fe fe-edit"></i></a>
									<a href="{{ action('ProductSizeTypeController@delete',[$vendor->alias , $product->alias , $product_size->id, $product_size_type->id])  }}" class="trash icon"><i class="fe fe-trash-2"></i></a>
{{--                                        <form action="{{action('ProductSizeController@delete',[$vendor->alias , $product->alias , $product_size->id]) }}" method="POST">--}}
{{--                                            @csrf--}}
{{--                                            @method('DELETE')--}}
{{--                                            <button class="btn trash icon" onclick="return confirm('Вы уверены?')"><i class="fe fe-trash-2"></i></button>--}}
{{--                                        </form>								--}}
                                </div>
							</li>
                            @endforeach
							<a class="add-list-btn" href="{{ action('ProductSizeTypeController@create',[$vendor->alias,$product->alias,$product_size->id]) }}">
								<i class="fe fe-plus-circle"></i>
								Добавить
							</a>
						</ul>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection

@section('script')
<script>
    $('.actioner').on('click',function () {
        let url = $(this).data('href');
        $.get(url).done(function(data){
            console.log(data);
        });
    })
</script>
@endsection
