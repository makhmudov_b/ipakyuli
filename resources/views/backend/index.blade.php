@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <h2 class="blue-title">Производители</h2>
            <div class="row">
                <form action="#" autocomplete="off">
                    <div class="white-block hotels">
                        <ul class="list-block">
                            @foreach($data as $vendor)
                                <li class="item">
                                    <a class="left-block" href="{{ action('ProductController@index',$vendor->alias) }}">
                                        <i class="fe fe-home"></i>
                                        <p class="pl-2">{{ $vendor->name  }}</p>
                                    </a>
                                    <div class="right-block">
                                        <a href="{{ action('ProductController@index',$vendor->alias) }}"
                                           class="btn btn-primary mr-5">Продукты</a>
                                        <span class="icon">
										<label class="custom-switch">
										  <input type="checkbox" name="custom-switch-checkbox"
                                                 class="custom-switch-input" @if($vendor->enabled) checked="checked" @endif>
										  <span class="custom-switch-indicator actioner"
                                                data-href="{{action('VendorController@switch',$vendor->alias)}}"></span>
										  <span class="custom-switch-description"></span>
										</label>
									</span>
                                        <a href="{{ action('VendorController@edit',$vendor->alias)  }}"
                                           class="edit icon"><i class="fe fe-edit"></i></a>
                                        <span class="trash icon"><i class="fe fe-trash-2"></i></span>
                                    </div>
                                </li>
                            @endforeach
                            @if(Auth::user()->hasRole('admin'))
                                <a class="add-list-btn" href="{{ action('VendorController@create') }}">
                                    <i class="fe fe-plus-circle"></i>
                                    Добавить обьект
                                </a>
                            @endif
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $('.actioner').on('click', function () {
            let url = $(this).data('href');
            $.get(url);
        })
    </script>
@endsection
