<nav class="nav d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">
        <div class="col-4 d-flex align-items-center">
            <div class="hamburger">
                <img src="{{asset('img/hamburger.svg')}}" alt="hamburger">
            </div>
            <a href="tel:+998712000308" class="phone">
                <img src="{{asset('img/phone.svg')}}" alt="phone">
            </a>
        </div>
        <div class="col-4 md-none align-items-center justify-content-center">
            <a href="{{action('PageController@index')}}" class="d-flex logo align-items-center justify-content-center">
                <img src="{{asset('img/logo.svg')}}" alt="logo">
            </a>
        </div>
        <div class="col-4 d-flex align-items-center justify-content-end">
            {{--            <div class="dollar nav-padding">--}}
            {{--                <img src="{{asset('img/dollar.svg')}}" alt="dollar">--}}
            {{--            </div>--}}
            @guest
                <a href="{{action('PageController@authorization')}}" class="profile nav-padding">
                    <img src="{{asset('img/profile.svg')}}" alt="profile">
                </a>
            @endguest
            @auth
                <a href="{{action('PageController@process')}}" class="profile nav-padding">
                    <img src="{{asset('img/profile.svg')}}" alt="profile">
                </a>
            @endauth
            <a href="{{action('PageController@search')}}" class="search nav-padding">
                <img src="{{asset('img/search.svg')}}" alt="search">
            </a>
            @guest
                <a class="basket" href="{{action('PageController@basket')}}">
                    <img src="{{asset('img/basket.svg')}}" alt="basket">
                </a>
            @endguest
            @auth
                <a class="basket relative" href="{{action('PageController@basket')}}">
                    <img src="{{asset('img/basket.svg')}}" alt="basket">
                    <span class="basket-count">@{{ basketCount }}</span>
                </a>
            @endauth
        </div>
    </div>
</nav>
<div class="left-navigation">
    <div class="top">
        <div class="d-flex justify-content-center align-items-center">
            <a href="{{action('PageController@index')}}">
                <img src="{{asset('img/logo.svg')}}" alt="logo">
            </a>
        </div>
        <a class="btn" href="{{action('PageController@categoryAll')}}">@lang('main.nav.7')</a>
        <ul class="listing">
            @foreach ($categories as $category)
                <li>
                    <a href="{{action('PageController@category',$category->alias)}}">{{ $category->name }}</a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="bottom">
        <div class="language">
            <a href="{{ LaravelLocalization::getLocalizedURL('uz') }}"
               class="uz {{ app()->getLocale() == 'uz' ? 'active' : '' }}">Uz</a>
            <a href="{{ LaravelLocalization::getLocalizedURL('ru') }}"
               class="ru {{ app()->getLocale() == 'ru' ? 'active' : '' }}">Ru</a>
        </div>
        <div class="d-flex align-items-end justify-content-between">
            <div>
                <div class="footer-title">@lang('main.nav.5'):</div>
                <div><a href="tel:+998712000308" class="footer-phone">+(99871) 200 03 08</a></div>
                <div><a class="footer-email" href="mailto:index@ipakyuli.com">index@ipakyuli.com</a></div>
            </div>
            <div>
                <img src="{{asset('img/indev.svg')}}" alt="indev">
            </div>
        </div>
    </div>
</div>
<div class="blur"></div>
