    <br><br>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-3 offers__block">
                <div class="offers__link d-flex align-items-center text-center">
                    <img src="{{asset('img/tracking.svg')}}" width="50" alt="icon">
                    <div class="offers__info" style="padding-left: 10px;">
                        <h4 class="offers__title text-center">Надежная доставка</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 offers__block">
                <div class="offers__link d-flex align-items-center text-center">
                    <img src="{{asset('img/contract.svg')}}" width="50" alt="icon">
                    <div class="offers__info" style="padding-left: 10px;">
                        <h4 class="offers__title text-center">Качественные продукты</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 offers__block">
                <div class="offers__link d-flex align-items-center text-center">
                    <img src="{{asset('img/support.svg')}}" width="50" alt="icon">
                    <div class="offers__info" style="padding-left: 10px;">
                        <h4 class="offers__title text-center">Онлайн поддержка</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 offers__block">
                <div class="offers__link d-flex align-items-center text-center">
                    <img src="{{asset('img/money.svg')}}" width="50" alt="icon">
                    <div class="offers__info" style="padding-left: 10px;">
                        <h4 class="offers__title text-center">Доступные цены</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
