<footer class="footer">
    <div class="container">
        <div class="footer-inner">
            <div class="footer-top">
                <div class="nav-middle__logo footer-top__title">
                    <a href="/"><img src="{{asset('img/logo.svg')}}" alt="logo">
                        <div style="align-items: center;display: flex;flex-direction: column;justify-content: center;">
                            <span class="logo-text">IpakYuli.com</span>
                            <span class="logo-text" style="font-size: 10px;justify-content: center;">Effective B2B Marketplace</span>
                        </div>
                    </a>
                </div>
            </div>
            <div class="footer-middle">

                <div class="footer-middle__navigation">
                    <div class="footer-middle__navigation--phone">
                        <div class="footer-middle__navigation--phone-info">@lang('main.nav.5'):</div>
                        <div class="footer-middle__navigation--phone-number">
                            <a href="tel:+998712000308">+(99871) 200 03 08</a>
                        </div>
                        <div class="footer-middle__navigation--phone-link">index@ipakyuli.com</div>
                    </div>
                    <div class="footer-middle__navigation--navs">
                        <div class="footer-middle__navigation--navs-title">Подробности:</div>
                        <div class="footer-middle__navigation--navs-list">
                            <ul>
                                <li><a href="{{action('PageController@about')}}">@lang('main.nav.3')</a></li>
                                <li><a href="{{action('PageController@rules')}}">@lang('main.nav.4')</a></li>
                                <li><a href="{{action('PageController@shipping')}}">@lang('main.nav.0')</a></li>
                                <li><a href="{{action('PageController@policy')}}">@lang('main.nav.1')</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="footer-bottom__license">IpakYuli © {{ \Carbon\Carbon::now()->year }}</div>
                <div class="footer-bottom__social">
                    <a href="#"><img src="{{ asset('img/telegram.svg')}}" alt=""></a><a href="#"><img
                            src="{{ asset('img/facebook.svg')}}" alt=""></a>
                </div>
                <div class="footer-bottom__developers">
                    <div class="developed">Developed by <span>InDev</span></div>
                    <a href="#"><img src="{{asset('img/Frame.svg')}}" alt="our-logo"></a>
                </div>
            </div>
        </div>
    </div>
</footer>
