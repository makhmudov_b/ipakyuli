<a class="product" href="{{action('PageController@showProduct',[$product->vendor->alias,$product->alias])}}">
    <img src="{{ asset($product->image[0]) }}" class="product__image" alt="product">
    <span class="absolute-hot">
        <img src="{{asset('img/hot.svg')}}" width="80" alt="hot">
    </span>
    @if($product->recommend)
        <span class="absolute-recommend">
        <img src="{{asset('img/recommend.svg')}}" width="80" alt="hot">
    </span>
    @endif
    <div class="product-bottom">
        <span class="product-name">{{$product->name}}</span>
        <div
            class="product-price">{{ count($product->range) > 1 ? number_format($product->range[0]).' - '.number_format($product->range[1])  . ' UZS': ($product->range[0] != 0 ? number_format($product->range[0]) . ' UZS': 'Договорная') }} </div>
        <div class="product-count">
            <span class="gray">@lang('main.index.1'):</span>
            <b class="bold">{{$product->min_count}}</b>
            <span class="gray">{{$product->count_type ?? 'шт'}}</span>
        </div>
    </div>
</a>
