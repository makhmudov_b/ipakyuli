<a class="product" href="{{action('PageController@showProduct',[$product->vendor->alias,$product->alias])}}">
    <div class="product__top">
        <img src="{{ asset($product->image[0]) }}" height="210" alt="product">
    </div>
    <div class="product__bottom">
        <div class="title">{{$product->name}}</div>
        <div class="price">{{ count($product->range) > 1 ? number_format($product->range[0]).' - '.number_format($product->range[1])  . ' UZS': ($product->range[0] != 0 ? number_format($product->range[0]) . 'UZS': 'Договорная') }} </div>
        <div class="count">
            <span class="gray">@lang('main.index.1'):</span>
            <b class="bold">{{$product->min_count}}</b>
            <span class="gray">{{$product->count_type ?? 'шт'}}</span>
        </div>
    </div>
</a>
