<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta
        name="description"
        content="Интернет магазин - всех принадлежностей"
    />
    <meta http-equiv="Content-Language" content="{{ app()->getLocale() }}"/>
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png"/>
    <link rel="stylesheet" href="{{ asset('css/styles.css')}}?ver=25"/>
    @yield('style')
    <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/notify.min.js')}}"></script>
    <script src="{{asset('js/slick-animation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
    <title>IpakYuli - Эффективная торговая площадка / Effective B2B Marketplace</title>
    <meta property="og:title" content="IpakYuli - Effective B2B Marketplace"/>
    <meta property="og:description" content="Effective B2B Marketplace - Эффективная торговая площадка"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{url()->full()}}"/>
    <meta property="og:image" content="/apple-touch-icon.png"/>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156085739-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-156085739-3');
    </script>

</head>
<body>
<div id="app" class="wrap">
    @include('partials.nav')
    <main role="main" class="main-wrap">
    @yield('content')
    </main>
    @include('partials.footer')
</div>
<script src="{{asset('js/vue.js')}}"></script>
@yield('script')
<script>
    let app = new Vue({
        el: '#app',
        data: {
            basketCount: 0,
            vendorId: 0
        },
        mounted() {
            let basket = JSON.parse(localStorage.getItem('basket'));
            if (basket != null) {
                if (basket.length > 0) {
                    let count = 0;
                    basket.forEach(function (e) {
                        count += e[0];
                    });
                    this.basketCount = count;
                }
            } else {
                localStorage.setItem('basket', JSON.stringify([]));
            }
            // basket  [count , prodId , sizeId , typeId , vendorId]
        },
        methods: {
            addItem: function (count, productId, sizeId, typeId, vendorId) {
                let basket = JSON.parse(localStorage.getItem('basket'));
                if (this.vendorId === 0) {
                    this.vendorId = vendorId;
                }
                if (this.vendorId === vendorId) {
                    basket.push([count, productId, sizeId, typeId, vendorId]);
                    this.basketCount = this.basketCount + count;
                    localStorage.setItem('basket', JSON.stringify(basket));
                    $.notify("Успешно добавлен ", "success");
                } else {
                    $.notify("Можете оформить заказ у одного производителя ", "error");
                }
                return 1;
            },
            removeBasketItem: function (count) {
                this.basketCount = this.basketCount - count;
                return 1;
            }
        }
    });
</script>
<script src="{{asset('js/main.js')}}?ver=12"></script>
</body>
</html>
