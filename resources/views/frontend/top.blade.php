@extends('layouts.frontend')

@section('content')
<div class="top-sale">
    <div class="container">
        <div class="category__all">Топ продаж</div>
        <div class="section-product result d-flex flex-wrap">
            @foreach($products as $product)
                @include('partials.product-premium')
            @endforeach
        </div>
    </div>
</div>
@endsection
