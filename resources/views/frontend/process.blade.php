@extends('layouts.frontend')

@section('content')
    <div class="container">
        <br><br>
        <div class="category__all">Профиль</div>
        <div class="profile-inner d-flex align-items-center justify-content-between">
            <div class="left d-flex align-items-center justify-content-between">
                <div class="black d-flex align-items-center justify-content-center">
                    <img src="{{asset('img/profile.svg')}}" alt="profile"/>
                </div>
                <div class="name">{{Auth::user()->name}}</div>
            </div>
            <div class="center">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="regular">В системе Ipakyuli:</div>
                    <div class="light">{{Auth::user()->created_at}}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="regular">Кол-во заказов:</div>
                    <div class="light">{{$orders->count()}}</div>
                </div>
            </div>
            <div class="right">
                <a href="{{action('PageController@messages')}}" class="btn main-btn">Центр собщений</a>
                <form class="d-inline-block" method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button class="btn danger-btn" type="submit"> Выйти</button>
                </form>
            </div>
        </div>
        <br><br>
        <div class="category__all">Мои заказы</div>
        <div class="my-orders">
            <div class="my-orders-wrapper">
                <div class="my-orders-top white-box">
                    <div class="my-orders-top__item">@lang('main.process.0')</div>
                    <div class="my-orders-top__item">@lang('main.process.1')</div>
                    <div class="my-orders-top__item">@lang('main.process.2')</div>
                </div>
                @foreach($orders as $order)
                    <div class="my-orders-bottom white-box">
                        <div class="my-orders-bottom__item--1">
                            @if($order->price * 1 != 0 && $order->status * 1 > 0)
                                <a href="{{action('OrderController@getOffer',$order->id)}}"
                                   class="my-orders-bottom__item--1_top disabled">@lang('main.process.3')
                                    №{{$order->id}}
                                    <svg style="margin-left: 10px;" xmlns="http://www.w3.org/2000/svg"
                                         height="20" version="1.1" viewBox="-53 1 511 511.99906">
                                        <g id="surface1">
                                            <path
                                                d="M 276.410156 3.957031 C 274.0625 1.484375 270.84375 0 267.507812 0 L 67.777344 0 C 30.921875 0 0.5 30.300781 0.5 67.152344 L 0.5 444.84375 C 0.5 481.699219 30.921875 512 67.777344 512 L 338.863281 512 C 375.71875 512 406.140625 481.699219 406.140625 444.84375 L 406.140625 144.941406 C 406.140625 141.726562 404.65625 138.636719 402.554688 136.285156 Z M 279.996094 43.65625 L 364.464844 132.328125 L 309.554688 132.328125 C 293.230469 132.328125 279.996094 119.21875 279.996094 102.894531 Z M 338.863281 487.265625 L 67.777344 487.265625 C 44.652344 487.265625 25.234375 468.097656 25.234375 444.84375 L 25.234375 67.152344 C 25.234375 44.027344 44.527344 24.734375 67.777344 24.734375 L 255.261719 24.734375 L 255.261719 102.894531 C 255.261719 132.945312 279.503906 157.0625 309.554688 157.0625 L 381.40625 157.0625 L 381.40625 444.84375 C 381.40625 468.097656 362.113281 487.265625 338.863281 487.265625 Z M 338.863281 487.265625 "
                                                style=" stroke:none;fill-rule:nonzero;fill:rgb(0%,0%,0%);fill-opacity:1;"/>
                                            <path
                                                d="M 305.101562 401.933594 L 101.539062 401.933594 C 94.738281 401.933594 89.171875 407.496094 89.171875 414.300781 C 89.171875 421.101562 94.738281 426.667969 101.539062 426.667969 L 305.226562 426.667969 C 312.027344 426.667969 317.59375 421.101562 317.59375 414.300781 C 317.59375 407.496094 312.027344 401.933594 305.101562 401.933594 Z M 305.101562 401.933594 "
                                                style=" stroke:none;fill-rule:nonzero;fill:rgb(0%,0%,0%);fill-opacity:1;"/>
                                            <path
                                                d="M 194.292969 357.535156 C 196.644531 360.007812 199.859375 361.492188 203.320312 361.492188 C 206.785156 361.492188 210 360.007812 212.347656 357.535156 L 284.820312 279.746094 C 289.519531 274.796875 289.148438 266.882812 284.203125 262.308594 C 279.253906 257.609375 271.339844 257.976562 266.765625 262.925781 L 215.6875 317.710938 L 215.6875 182.664062 C 215.6875 175.859375 210.121094 170.296875 203.320312 170.296875 C 196.519531 170.296875 190.953125 175.859375 190.953125 182.664062 L 190.953125 317.710938 L 140 262.925781 C 135.300781 257.980469 127.507812 257.609375 122.5625 262.308594 C 117.617188 267.007812 117.246094 274.800781 121.945312 279.746094 Z M 194.292969 357.535156 "
                                                style=" stroke:none;fill-rule:nonzero;fill:rgb(0%,0%,0%);fill-opacity:1;"/>
                                        </g>
                                    </svg>
                                </a>
                            @endif
                            <div class="my-orders-bottom__item--1_bottom">{{$order->vendor->name}}</div>
                        </div>
                        <div class="my-orders-bottom__item--2">
                            <div class="my-orders-bottom__item--2_wrapper">
                                @foreach($order->items as $order_items)
                                    <div class="my-orders-bottom__item--2_product">
                                        <div class="my-orders-bottom__item--2_product-name">
                                            <div class="my-orders-bottom__item--2_product-name__img"></div>
                                            <div
                                                class="my-orders-bottom__item--2_product-name__title">{{$order_items->name}} </div>
                                        </div>
                                        <div class="my-orders-bottom__item--2_product-quantity">{{$order_items->amount}}
                                            шт
                                        </div>
                                        <div class="my-orders-bottom__item--2_product-cost">
                                            {{$order_items->price == 0 ? 'Договорная' : $order_items->price .' UZS'}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="my-orders-bottom__item--3">
                            <div class="top">
                                <div class="status" style="text-align: center;">
                                    @if($order->status == -1) @lang('main.process.4') @endif
                                    @if($order->status == 0) @lang('main.process.5') @endif
                                    @if($order->status == 0)
                                        <p>@lang('main.process.6')</p>
                                    @endif
                                    @if($order->status == 1) @lang('main.process.7') @endif
                                    @if($order->status == 2) @lang('main.process.8') @endif
                                    @if($order->status == 3) @lang('main.process.9') @endif
                                    @if($order->status == 4) @lang('main.process.10') @endif
                                </div>
                            </div>
                            @if($order->status != 4)
                                <div class="bottom">
                                    @if($order->status == 0)
                                        @if($order->price *1 != 0 )
                                            <a href="{{action('PageController@updateOrder',[$order->id,0])}}"
                                               onclick="return confirm('@lang('main.process.11')?')"
                                               class="status">@lang('main.process.12')</a>
                                        @else
                                            <a href="{{action('PageController@message',$order->id)}}"
                                               class="status">@lang('main.process.13')</a>
                                        @endif
                                    @endif
                                    @if($order->status == 1)
                                        @if(count($order->getShipping()))
                                            <form class="status"
                                                  action="{{action('PageController@chooseCarrier',[$order->id,1])}}"
                                                  method="POST">
                                                @csrf
                                                @method('PUT')
                                                <div class="choice">
                                                    <div class="form-control">
                                                        <label for="options">@lang('main.process.14')</label>
                                                        <select class="select" name="offer_id" id="options">
                                                            <option value="-1">@lang('main.process.15')</option>
                                                            @foreach($order->getShipping()[0]->offer as $offer)
                                                                <option
                                                                    value="{{$offer->id}}">{{number_format($offer->price)}}
                                                                    UZS - {{$offer->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <button onclick="return confirm('@lang('main.process.16')?')">
                                                        @lang('main.process.17')
                                                    </button>
                                                </div>
                                            </form>
                                        @endif
                                    @endif
                                    @if($order->status == 2)
                                        <a href="{{action('PageController@updateOrder',[$order->id , 3])}}"
                                           onclick="return confirm('@lang('main.process.18')?')"
                                           class="status">@lang('main.process.19')</a>
                                    @endif
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <br><br>
@endsection
@section('script')
    @if(!isset($clearBasket))
        <script>
            let basket = JSON.parse(localStorage.getItem('basket'));
            if (basket.length > 0) {
                localStorage.setItem('basket', JSON.stringify([]));
                $.notify("@lang('main.process.20') ", "success");
            }
        </script>
    @endif
@endsection
