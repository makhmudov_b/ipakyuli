@extends('layouts.frontend')

@section('content')
    <div class="collections-wrap auth-wrap d-flex align-items-center justify-content-center">
        <div class="container">
            <form action="{{action('AuthController@register')}}" method="POST" class="auth">
                @csrf
                @method('PUT')
                <img src="{{asset('img/logo.svg')}}" alt="logo">
                <div class="phone d-flex align-items-center justify-content-between">
                    <input type="text" name="username" placeholder="Ведите ваше Ф.И.О"  required="required"/>
                </div>
                <button class="btn" type="submit">Продолжить</button>
            </form>
        </div>
    </div>
@endsection
