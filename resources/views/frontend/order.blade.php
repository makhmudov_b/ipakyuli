@extends('layouts.frontend')

@section('content')
    <div class="container">
        <div class="order main-section">
            <div class="order-wrapper main-section-wrapper">
                <div class="main-section-process">
                    <div class="order-inner">
                        <div class="order-inner__main">
                            <div class="main-section-process-title order-inner__main--title">
                                <div class="main-section-process-title__name adress-shipping ">
                                    <div class="main-section-process-title__name--img"><img src="../img/Layer 2 (1).svg" alt=""></div>
                                    <div class="main-section-process-title__name--title">Адрес доставки</div>
                                </div>
                            </div>
                            <div class="main-section-process-action current order-inner__main--action">
                                <div class="current__adress">
                                    <div class="current__adress--top">
                                        <div class="current__adress--top_title">г. Ташкент, Мирабадский район, ул. Авлоний, дом 4, кв 5</div>
                                        <div class="current__adress--top_subtitle">В махалью можно заехать толко с заднего входа, охранику дайте мой номер <br> Код подезда 456, 4 этаж, с лева серая дверь</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order-inner__main">
                            <div class="main-section-process-title">
                                <div class="main-section-process-title__name">
                                    <div class="main-section-process-title__name--img"><img src="../img/Layer 2 (2).svg" alt=""></div>
                                    <div class="main-section-process-title__name--title">Метод оплаты</div>
                                </div>
                            </div>
                            <div class="main-section-process-action current order-inner__main--payment">
                                <div class="two-top order-inner__main--payment_onlinecard">
                                    <input type="radio" name="money" id="money1" checked="checked" />
                                    <label for="money1" class="order-inner__main--payment_onlinecard--label">Онлайн картой</label>
                                </div>
                                <div class="two-middle order-inner__main--payment_card">
                                    <input type="radio" name="card" id="card1" checked="checked" />
                            <label class="two-middle__cards" for="card1">
                                <img src="../img/Rectangle 40.png" alt="">
                                <span class="two-middle__cards--name">Master Card</span>
                            </label>
                                    <div class="confirm">Подтверждено</div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="main-section-process-title">
                        <div class="main-section-process-title__name">
                            <div class="main-section-process-title__name--img"><img src="../img/Layer 2 (1).png" alt=""></div>
                            <div class="main-section-process-title__name--title">Корзина товаров </div>
                        </div>
                    </div>
                    <div class="main-section-process-action third current">
                        <div class="third-wrapper">
                            <input type="checkbox" id="checkbox">
                            <label for="checkbox" class="third-item">
                                <div class="third-item__img"></div>
                                <div class="third-item__description">
                                    <div class="third-item__description--info">
                                        <div class="item-info">
                                            <div class="item-info__top">
                                                <div class="item-info__top--title">Платье Кокетка </div>
                                                <div class="item-info__top--subtitle">OOO Cotton Club</div>
                                            </div>
                                            <div class="item-info__bottom order-rating">
                                                <div class="item-info__bottom--img">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 5.svg')}}" alt="">
                                                </div>
                                                <div class="item-info__bottom--cost">4 560 USD</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="third-item__description--descript">
                                        <div class="third-item__description--descript_title">Описание товара</div>
                                        <div class="third-item__description--descript_text">состояние: 100% Brand New И Высокое Качество <br>
                                        материал: Полиэстер
                                        цвета: Белый, Черный, Красный, Синий, Зеленый
                                    </div>
                                    </div>
                                    <div class="third-item__description--countity">
                                        <div class="third-item__description--countity_quantity">
                                            <div class="third-item__description--countity_quantity-title">Количество:</div>
                                            <div class="third-item__description--countity_quantity-navs">
                                                <button><img src="../img/Group 55.svg" alt=""></button>
                                                <div class="number">1</div>
                                                <button><img src="{{ asset('img/Group 54.svg') }}" alt=""></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="third-item__udalit">
                                    <div class="third-item__udalit--link">
                                        <button><img src="{{asset('img/Layer 2 (3).svg')}}" alt=""></button>
                                    </div>
                                </div>
                            </label>
                        </div>
                        <div class="third-wrapper">
                            <input type="checkbox" id="checkbox2">
                            <label for="checkbox2" class="third-item">
                                <div class="third-item__img"></div>
                                <div class="third-item__description">
                                    <div class="third-item__description--info">
                                        <div class="item-info">
                                            <div class="item-info__top">
                                                <div class="item-info__top--title">Платье Кокетка </div>
                                                <div class="item-info__top--subtitle">OOO Cotton Club</div>
                                            </div>
                                            <div class="item-info__bottom order-rating">
                                                <div class="item-info__bottom--img">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 5.svg')}}" alt="">
                                                </div>
                                                <div class="item-info__bottom--cost">4 560 USD</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="third-item__description--descript">
                                        <div class="third-item__description--descript_title">Описание товара</div>
                                        <div class="third-item__description--descript_text">состояние: 100% Brand New И Высокое Качество <br>
                                        материал: Полиэстер
                                        цвета: Белый, Черный, Красный, Синий, Зеленый
                                    </div>
                                    </div>
                                    <div class="third-item__description--countity">
                                        <div class="third-item__description--countity_quantity">
                                            <div class="third-item__description--countity_quantity-title">Количество:</div>
                                            <div class="third-item__description--countity_quantity-navs">
                                                <button><img src="../img/Group 55.svg" alt=""></button>
                                                <div class="number">1</div>
                                                <button><img src="{{ asset('img/Group 54.svg') }}" alt=""></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="third-item__udalit"><div class="third-item__udalit--link"><button><img src="../img/Layer 2 (3).svg" alt=""></button></div></div>
                            </label>
                        </div>
                        <div class="third-wrapper">
                            <input type="checkbox" id="checkbox3">
                            <label for="checkbox3" class="third-item">
                                <div class="third-item__img"></div>
                                <div class="third-item__description">
                                    <div class="third-item__description--info">
                                        <div class="item-info">
                                            <div class="item-info__top">
                                                <div class="item-info__top--title">Платье Кокетка </div>
                                                <div class="item-info__top--subtitle">OOO Cotton Club</div>
                                            </div>
                                            <div class="item-info__bottom order-rating">
                                                <div class="item-info__bottom--img">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 5.svg')}}" alt="">
                                                </div>
                                                <div class="item-info__bottom--cost">4 560 USD</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="third-item__description--descript">
                                        <div class="third-item__description--descript_title">Описание товара</div>
                                        <div class="third-item__description--descript_text">состояние: 100% Brand New И Высокое Качество <br>
                                        материал: Полиэстер
                                        цвета: Белый, Черный, Красный, Синий, Зеленый
                                    </div>
                                    </div>
                                    <div class="third-item__description--countity">
                                        <div class="third-item__description--countity_quantity">
                                            <div class="third-item__description--countity_quantity-title">Количество:</div>
                                            <div class="third-item__description--countity_quantity-navs">
                                                <button><img src="../img/Group 55.svg" alt=""></button>
                                                <div class="number">1</div>
                                                <button><img src="{{ asset('img/Group 54.svg') }}" alt=""></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="third-item__udalit"><div class="third-item__udalit--link"><button><img src="../img/Layer 2 (3).svg" alt=""></button></div></div>
                            </label>
                        </div>
                        <div class="third-wrapper">
                            <input type="checkbox" id="checkbox4">
                            <label for="checkbox4" class="third-item">
                                <div class="third-item__img"></div>
                                <div class="third-item__description">
                                    <div class="third-item__description--info">
                                        <div class="item-info">
                                            <div class="item-info__top">
                                                <div class="item-info__top--title">Платье Кокетка </div>
                                                <div class="item-info__top--subtitle">OOO Cotton Club</div>
                                            </div>
                                            <div class="item-info__bottom order-rating">
                                                <div class="item-info__bottom--img">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 5.svg')}}" alt="">
                                                </div>
                                                <div class="item-info__bottom--cost">4 560 USD</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="third-item__description--descript">
                                        <div class="third-item__description--descript_title">Описание товара</div>
                                        <div class="third-item__description--descript_text">состояние: 100% Brand New И Высокое Качество <br>
                                        материал: Полиэстер
                                        цвета: Белый, Черный, Красный, Синий, Зеленый
                                    </div>
                                    </div>
                                    <div class="third-item__description--countity">
                                        <div class="third-item__description--countity_quantity">
                                            <div class="third-item__description--countity_quantity-title">Количество:</div>
                                            <div class="third-item__description--countity_quantity-navs">
                                                <button><img src="../img/Group 55.svg" alt=""></button>
                                                <div class="number">1</div>
                                                <button><img src="{{ asset('img/Group 54.svg') }}" alt=""></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="third-item__udalit"><div class="third-item__udalit--link"><button><img src="../img/Layer 2 (3).svg" alt=""></button></div></div>
                            </label>
                        </div>
                        <div class="third-wrapper">
                            <input type="checkbox" id="checkbox5">
                            <label for="checkbox5" class="third-item">
                                <div class="third-item__img"></div>
                                <div class="third-item__description">
                                    <div class="third-item__description--info">
                                        <div class="item-info">
                                            <div class="item-info__top">
                                                <div class="item-info__top--title">Платье Кокетка </div>
                                                <div class="item-info__top--subtitle">OOO Cotton Club</div>
                                            </div>
                                            <div class="item-info__bottom order-rating">
                                                <div class="item-info__bottom--img">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 5.svg')}}" alt="">
                                                </div>
                                                <div class="item-info__bottom--cost">4 560 USD</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="third-item__description--descript">
                                        <div class="third-item__description--descript_title">Описание товара</div>
                                        <div class="third-item__description--descript_text">состояние: 100% Brand New И Высокое Качество <br>
                                        материал: Полиэстер
                                        цвета: Белый, Черный, Красный, Синий, Зеленый
                                    </div>
                                    </div>
                                    <div class="third-item__description--countity">
                                        <div class="third-item__description--countity_quantity">
                                            <div class="third-item__description--countity_quantity-title">Количество:</div>
                                            <div class="third-item__description--countity_quantity-navs">
                                                <button><img src="../img/Group 55.svg" alt=""></button>
                                                <div class="number">1</div>
                                                <button><img src="{{ asset('img/Group 54.svg') }}" alt=""></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="third-item__udalit"><div class="third-item__udalit--link"><button><img src="../img/Layer 2 (3).svg" alt=""></button></div></div>
                            </label>
                        </div>
                        <div class="third-wrapper">
                            <input type="checkbox" id="checkbox6">
                            <label for="checkbox6" class="third-item">
                                <div class="third-item__img"></div>
                                <div class="third-item__description">
                                    <div class="third-item__description--info">
                                        <div class="item-info">
                                            <div class="item-info__top">
                                                <div class="item-info__top--title">Платье Кокетка </div>
                                                <div class="item-info__top--subtitle">OOO Cotton Club</div>
                                            </div>
                                            <div class="item-info__bottom order-rating">
                                                <div class="item-info__bottom--img">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 3.svg')}}" alt="">
                                                    <img src="{{asset('img/Star 5.svg')}}" alt="">
                                                </div>
                                                <div class="item-info__bottom--cost">4 560 USD</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="third-item__description--descript">
                                        <div class="third-item__description--descript_title">Описание товара</div>
                                        <div class="third-item__description--descript_text">состояние: 100% Brand New И Высокое Качество <br>
                                        материал: Полиэстер
                                        цвета: Белый, Черный, Красный, Синий, Зеленый
                                    </div>
                                    </div>
                                    <div class="third-item__description--countity">
                                        <div class="third-item__description--countity_quantity">
                                            <div class="third-item__description--countity_quantity-title">Количество:</div>
                                            <div class="third-item__description--countity_quantity-navs">
                                                <button><img src="../img/Group 55.svg" alt=""></button>
                                                <div class="number">1</div>
                                                <button><img src="{{ asset('img/Group 54.svg') }}" alt=""></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="third-item__udalit"><div class="third-item__udalit--link"><button><img src="/img/Layer 2 (3).svg" alt=""></button></div></div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection
