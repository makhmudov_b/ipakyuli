@extends('layouts.frontend')

@section('content')
    <div class="container">
        <br>
        <div class="product-informations">
            <div class="product-informations-wrapper white-box">
                <div class="product-title"><span>{{$vendor->name}} </span></div>
                <div class="product-informations-middle">
                    <div class="document-slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                @foreach($vendor->image as $image)
                                    <div class="swiper-slide">
                                        <img src="{{ asset($image) }}" height="150" alt="vendor">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-informations-middle__info">
                <div class="information flex-direction-column">
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Тип Бизнеса </div>
                        <div class="info"> @foreach($vendor->categories as $category) {{$category->name}} @endforeach </div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Основные продукты </div>
                        <div class="info"> @foreach($vendor->categories as $category) {{$category->name}} @endforeach </div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Гувохнома </div>
                        <div class="info"> - </div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Сертификаты </div>
                        <div class="info"> - </div>
                    </div>
                </div>
                <div class="information flex-direction-column">
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Патенты </div>
                        <div class="info"> - </div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Страна - Регион </div>
                        <div class="info"> {{$vendor->country->name}} - {{$vendor->city->name}}</div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Кол-во сотрудников </div>
                        <div class="info"> {{$vendor->workers}} </div>
                    </div>
                    <div class="block d-flex align-items-center ">
                        <div class="naming"> Год основания </div>
                        <div class="info"> {{$vendor->open_date}} </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="section-product">
            @foreach($related as $inner)
                <div class="title-divider d-flex align-items-center justify-content-between">
                    <div class="main-title">{{$inner->name}}</div>
                </div>
                <div class="main-card">
                    @foreach($products->where('category_id',$inner->id) as $product)
                        @include('partials.product')
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
    <br><br>
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('css/swiper.css')}}">
@endsection
@section('script')
    <script src="{{asset('js/swiper.js')}}"></script>
    <script>
        $(document).ready(function () {
            const swiper = new Swiper('.swiper-container', {
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        });
    </script>
@endsection
