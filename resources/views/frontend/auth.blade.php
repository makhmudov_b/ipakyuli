@extends('layouts.frontend')

@section('content')
    <div class="container">
        <div class="section-register">
            <div class="section-register-wrapper">
                <form action="{{action('AuthController@register')}}" method="POST" class="section-register-form">
                    @csrf
                    <div class="section-register-form__field first-field">
                    <legend class="section-register-form__field--legend">@lang('main.auth.0')</legend>
                        <div class="section-register-form__field--input">
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="name">@lang('main.auth.1')</label>
                                    <input type="text" id="name" name="name" />
                                </div>
                            </div>
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="mail">@lang('main.auth.2')</label>
                                    <input type="email" id="mail" name="email" placeholder="" />
                                </div>
                            </div>
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="phone">@lang('main.auth.3')</label>
                                    <input type="tel" id="phone" name="phone" placeholder="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-register-form__field first-field">
                        <div class="section-register-form__field--input">
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="city">@lang('main.auth.4')</label>
                                    <input type="text" id="city" name="city" />
                                </div>
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="country">@lang('main.auth.5')</label>
                                    <input type="text" id="country" name="country" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-register-form__field first-field">
                    <legend class="section-register-form__field--legend">@lang('main.auth.6')</legend>
                        <div class="section-register-form__field--input">
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="password">@lang('main.auth.7')</label>
                                    <input type="password" id="password" name="password" />
                                </div>
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="confirm_password">@lang('main.auth.8')</label>
                                    <input type="password" id="confirm_password" name="password_confirmation" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-register-form__submit">
                        <input type="submit" value="@lang('main.auth.9')">
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
