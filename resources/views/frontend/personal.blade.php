@extends('layouts.frontend')

@section('content')
    <div class="container">
        <div class="personal">
            <div class="personal-item">
                <div class="personal-item__data">
                    <div class="personal-item__data--info">
                        <div class="personal-item__data--info_title">Личные данные</div>
                        <div class="personal-item__data--info_person">
                            <div class="personal-item__data--info_person-left">Ф.И.О.</div>
                            <div class="personal-item__data--info_person-right">{{$data->name}}</div>
                        </div>
                        <div class="personal-item__data--info_person">
                            <div class="personal-item__data--info_person-left">Дата рождения</div>
                            <div class="personal-item__data--info_person-right">{{$data->birthday ?? '1999.01.01'}}</div>
                        </div>
                        <div class="personal-item__data--info_person">
                            <div class="personal-item__data--info_person-left">Номер телефона</div>
                            <div class="personal-item__data--info_person-right">+{{$data->phone}}</div>
                        </div>
                        <div class="personal-item__data--info_person">
                            <div class="personal-item__data--info_person-left">Почта</div>
                            <div class="personal-item__data--info_person-right">{{$data->email}}</div>
                        </div>
                    </div>
                    <div class="personal-item__data--form">
                        <div class="personal-item__data--form_title content-title">Изменить пароль</div>
                        <form action="">
                            <input type="text" placeholder="Новый пароль">
                            <input type="text" placeholder="Повторите пароль">
                            <input type="button" value="Сохранить" class="personal-item__data--form_input green-button">
                        </form>
                    </div>
                </div>
            </div>
            <div class="personal-item">
                <div class="main-section-process-title__name adress-shipping personal-item__top">
                    <div class="main-section-process-title__name--img personal-item__top--img"><img src="{{ asset('img/Layer 2 (1).svg')}}" alt=""></div>
                    <div class="main-section-process-title__name--title personal-item__top--title">Адрес доставки</div>
                </div>
                <div class="current__adress">
                    <div class="current__adress--top">
                        <label for="optionsRadios1" class="current__adress--top_title">г. Ташкент, Мирабадский район, ул. Авлоний, дом 4, кв 5</label>
                        <div class="current__adress--top_subtitle">В махалью можно заехать толко с заднего входа, охранику дайте мой номер <br> Код подезда 456, 4 этаж, с лева серая дверь</div>
                        <div class="current__adress--top_links">
                            <button href="#" class="personal-item-edit current__adress--top_links-edit">Редактировать</button><button href="#" class="personal-item-remove current__adress--top_links-remove">Удалить</button>
                        </div>
                    </div>
                </div>
                <div class="current__adress">
                    <div class="current__adress--top">
                        <label for="optionsRadios1" class="current__adress--top_title">г. Ташкент, Мирабадский район, ул. Авлоний, дом 4, кв 5</label>
                        <div class="current__adress--top_subtitle">В махалью можно заехать толко с заднего входа, охранику дайте мой номер <br> Код подезда 456, 4 этаж, с лева серая дверь</div>
                        <div class="current__adress--top_links">
                            <button href="#" class="personal-item-edit current__adress--top_links-edit">Редактировать</button><button href="#" class="personal-item-remove current__adress--top_links-remove">Удалить</button>
                        </div>
                    </div>
                </div>
                <div class="current__adress--bottom  personal-item__button">
                    <a href="#">+ Добавить новый адрес</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
{{--<script src="{{asset('js/main.js')}}"></script>--}}
@endsection
