@extends('layouts.frontend')

@section('content')
    <br>
    <div class="category">
        <div class="container">
            <div class="category__all">@lang('main.nav.7')</div>
            <div class="d-flex flex-wrap category__wrap justify-content-between">
                @foreach($categories as $category)
                    <a href="{{action('PageController@category',$category->alias)}}" class="category__block">
                        <img src="{{asset($category->image)}}" alt="image"/>
                        <span class="title">
                    <span class="inside"> {{$category->name}}</span>
                </span>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection
