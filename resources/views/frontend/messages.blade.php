@extends('layouts.frontend')

@section('content')
    <br>
    <div class="container">
        <div class="category__all">@lang('main.messages.0')</div>
        <br>
        <div class="messages">
            @foreach($messages as $message)
                <a
                    href="{{action('PageController@message',$message->order_id)}}"
                    class="block">
                    <div class="image">
                        <img src="" alt="">
                    </div>
                    <div class="info">
                        <div class="flex-1">
                            <div class="bold">№ {{$message->order_id}}</div>
                            <div class="bold m-10">{{$message->order->order_item[0]->name}}</div>
                            <div class="light m-10">{{$message->body}}</div>
                        </div>
                        <div class="date">
                            <div class="light">{{\Carbon\Carbon::parse($message->created_at)->format('d-m-yy')}}</div>
                            {{--                            <div class="count m-10">5</div>--}}
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
    <br>
@endsection
