@extends('layouts.frontend')

@section('content')
    <div class="slider">
        @foreach($banners as $banner)
            <a href="{{$banner->name_uz}}" target="_blank" class="slider-inner">
                {{--                @if($banner->vendor_id * 1 > 0 || $banner->product_id * 1 > 0)--}}
                {{--                    @if($banner->vendor_id * 1 > 0)--}}
                {{--                        <div>--}}
                {{--                            <a href="/vendor">--}}
                {{--                                <img--}}
                {{--                                    src="{{asset($banner->image[0])}}"--}}
                {{--                                    alt="image"--}}
                {{--                                />--}}
                {{--                            </a>--}}
                {{--                        </div>--}}
                {{--                    @elseif($banner->product_id * 1 > 0 )--}}
                {{--                        <div>--}}
                {{--                            <a href="/product">--}}
                {{--                                <img--}}
                {{--                                    src="{{asset($banner->image[0])}}"--}}
                {{--                                    alt="image"--}}
                {{--                                />--}}
                {{--                            </a>--}}
                {{--                        </div>--}}
                {{--                    @endif--}}
                {{--                @else--}}
                {{--                    <a href="shit" class="full-width">--}}
                {{--                        <img--}}
                {{--                            src="{{asset($banner->image[0])}}"--}}
                {{--                            alt="image"--}}
                {{--                        />--}}
                {{--                    </a>--}}
                {{--                @endif--}}
                <img
                    src="{{asset($banner->image[0])}}"
                    alt="image"
                />
            </a>
        @endforeach
    </div>
    <div class="categories">
        <img src="{{asset('img/category-back.svg')}}" class="categories__back" alt="back-photo">
        <div class="container">
            <div class="categories__slider">
                @foreach($categories as $category)
                    <a href="{{action('PageController@category',$category->alias)}}" class="item">
                        <img src="{{asset($category->image)}}" class="item-image" alt="">
                        <div class="name">{{$category->name}}</div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section-product">
        <div class="container">
            <div class="section-product-wrapper">
                <div class="title-divider d-flex align-items-center justify-content-between">
                    <div class="main-title">Топы продаж</div>
                    <a href="{{action('PageController@top')}}" class="main-all">
                        @lang('main.index.0') <img src="{{asset('img/arrow-right.svg')}}" alt="arrow"/>
                    </a>
                </div>
                <div class="big-card d-flex align-items-center justify-content-between flex-wrap">
                    @foreach($hots as $key => $product)
                        @if($key < 2)
                            @include('partials.big-cards')
                        @else
                            @break
                        @endif
                    @endforeach
                </div>
                <div class="main-card">
                    @foreach($hots as $key => $product)
                        @if($key >= 2)
                            @include('partials.product-premium')
                        @endif
                    @endforeach
                </div>
                @foreach($categories as $key => $category)
                    <div class="title-divider d-flex align-items-center justify-content-between">
                        <div class="main-title">{{$category->name}}</div>
                        <a href="{{action('PageController@category',$category->alias)}}" class="main-all">
                            @lang('main.index.0') <img src="{{asset('img/arrow-right.svg')}}" alt="arrow"/>
                        </a>
                    </div>
                    <div class="main-card">
                        @foreach($category->main_products() as $key => $product)
                            @include('partials.product')
                        @endforeach
                    </div>
                    @if($key < 5)
                        @if($collections[$key] ?? false)
                            <div class="title-divider d-flex align-items-center justify-content-between">
                                <div class="main-title">Сборники</div>
                                <a href="{{action('PageController@collection')}}" class="main-all">
                                    @lang('main.index.0') <img src="{{asset('img/arrow-right.svg')}}" alt="arrow"/>
                                </a>
                            </div>
                            <div class="collections d-flex align-items-center justify-content-between">
                                <div class="collections-inner"
                                     style="background-image: url('{{$collections[$key]->image}}')">
                                    <div class="title">{{$collections[$key]->name}}</div>
                                    <div class="description">{{$collections[$key]->description}}</div>
                                    <a href="{{action('PageController@showCollection',$collections[$key]->id)}}"
                                       class="btn">Посмотреть больше</a>
                                </div>
                                <div class="collections-right d-flex align-items-center justify-content-end">
                                    @foreach($collections[$key]->products as $inner => $product)
                                        @if($inner < 4)
                                            @include('partials.product')
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <br><br>
@endsection
