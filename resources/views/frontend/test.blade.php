<html>
<head>
<title>How to draw Path on Map using Google Maps Direction API</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<style>
#map-layer {
    max-width: 900px;
    min-height: 550px;
    margin: 0 auto;
}
</style>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
</head>
<body>

    <div class="container d-flex align-items-center justify-content-center pt-5 pb-5">
        <div class="locations-option"><input type="text" name="way_points[]" class="way_points" value="41.311081 69.240562"></div>
        <div class="locations-option pl-1"><input type="text" name="way_points[]" class="way_points" value="41.411081 69.270562"></div>
        <input type="button" id="drawPath" value="Draw Path" class="btn btn-primary ml-2" />
    </div>

    <div id="map-layer"></div>
    <script>
      	var map;
		var waypoints
      	function initMap() {
        	  	var mapLayer = document.getElementById("map-layer");
            	var centerCoordinates = new google.maps.LatLng(41.311081, 69.240562);
        		var defaultOptions = { center: centerCoordinates, zoom: 12 , disableDefaultUI: true };
        		map = new google.maps.Map(mapLayer, defaultOptions);

            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            directionsDisplay.setMap(map);

            $("#drawPath").on("click",function() {
            	    waypoints = Array();
                	$('.way_points').each(function() {
                    waypoints.push({
                        location: $(this).val(),
                        stopover: true
                    });
                	});
                var locationCount = waypoints.length;
                if(locationCount > 0) {
                    var start = waypoints[0].location;
                    var end = waypoints[locationCount-1].location;
                    drawPath(directionsService, directionsDisplay,start,end);
                }
            });

      	}
        	function drawPath(directionsService, directionsDisplay,start,end) {
            directionsService.route({
              origin: start,
              destination: end,
              waypoints: waypoints,
              optimizeWaypoints: true,
              travelMode: 'WALKING'
            }, function(response, status) {
                if (status === 'OK') {
                directionsDisplay.setDirections(response);
                } else {
                window.alert('Problem in showing direction due to ' + status);
                }
            });
      }
	</script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&&callback=initMap">
    </script>
</body>
</html>
