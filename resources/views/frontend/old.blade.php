@extends('layouts.frontend')

@section('content')
<div class="container">
    <div class="section-second">
        <div class="section-second-wrapper">
            <div class="section-second__item section-second__item--1">
            <div class="section-second__item section-second__item--1_category">
                <div class="section-second__item--1_category-title">Категории</div>
                <div class="section-second__item--1_category-menu">
                    <ul>
                        @foreach ($categories as $category)
                        <li>
                            <a href="{{action('PageController@category',$category->alias)}}"><span class="list-item">{{ $category->name }}</span> <span class="direction">></span></a>
                            <div class="menu-second">
                                <ul>
                                    @foreach ($category->sub_category as $sub_category)
                                        <li><a href="{{ action('PageController@category',$sub_category->alias) }}"><span class="list-item">{{$sub_category->name_ru}}</span> <span class="direction">></span></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
        <div class="section-second__item section-second__item--2">
            <div class="slider-fixer">
                <div class="main-slider">
                    @foreach($banners as $banner)
                    <div class="main-slider__item">
                        <img src="{{$banner->image[0]}}" alt="banner" style="width: 100%;object-fit: contain;" />
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
            <div class="section-second__item section-second__item--3">
                <div class="stock">
                    <div class="stock-title">Акции и скидки</div>
                    @foreach($products as $key => $product)
                    @if($key == 3) @break @endif
                    <div class="stock-content">
                        <div class="stock-content__img">
                            <div class="stock-content__img--insider" style="background: url({{asset($product->image[0])}}) no-repeat center; background-size: cover; border-radius: 5px;"></div>
                        </div>
                        <div class="stock-content__text">
                            <div class="stock-content__text--product">
                                <a href="{{action('PageController@showProduct',[$product->vendor->alias,$product->alias])}}">
                                    <span class="stock-content__text--product_product-name">{{$product->name}} </span>
                                    <span class="stock-content__text--product_product-maker">{{$product->vendor->name}}</span>
                                </a>
                            </div>
                            <div class="stock-content__text--price">
                                <div class="stock-content__text--price_price-percent">-50%</div>
                                <div class="stock-content__text--price_price-cost">
                                    {{ $product->current }} UZS
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="section-product">
        <div class="section-product-wrapper">
            <div class="main-title">Текстильные изделия
                <span class="red-item">
                    <a href="#"> Все <span class="tire">></span></a>
                </span>
            </div>
            <div class="main-card">
                @foreach($products as $key => $product)
                <a class="main-card__item" href="{{action('PageController@showProduct',[$product->vendor->alias,$product->alias])}}">
                    <div class="main-card__item--images big-cards__item-bottom--prodaj">
                        <img src="img/Star 3.svg" alt="star">
                        <img src="img/Star 3.svg" alt="star">
                        <img src="img/Star 3.svg" alt="star">
                        <img src="img/Star 3.svg" alt="star">
                        <img src="img/Star 5.svg" alt="star">
                    </div>
                    <div class="main-card__item--stock">
                        <div class="main-card__item--stock_percent">-25%</div>
                    </div>
                    <div class="main-card__item--img"><img src="{{ asset($product->image[0]) }}" alt="photo" /></div>
                    <div class="main-card__item--item-info">
                        <div class="main-card__item--item-info__top">
                            <span class="main-card__item--item-info__top--title">{{$product->name}}</span>
                            <span class="main-card__item--item-info__top--subtitle">{{$product->vendor->name}}</span>
                        </div>
                        <div class="main-card__item--item-info__bottom">
                            <div class="main-card__item--item-info__bottom--cost">{{ $product->current  }} UZS</div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
            <div class="main-title">Бытовая техника<span class="red-item"><a href="#"> Все <span class="tire">></span></a></span></div>
            <div class="main-card">
                @foreach($products as $key => $product)
                <a class="main-card__item" href="{{action('PageController@showProduct',[$product->vendor->alias,$product->alias])}}">
                    <div class="main-card__item--stock">
                        <div class="main-card__item--stock_percent">-25%</div>
                    </div>
                    <div class="main-card__item--img" style="background: url({{asset($product->image[0])}}) no-repeat center; background-size: cover;"></div>
                    <div class="main-card__item--item-info">
                        <div class="main-card__item--item-info__top">
                            <span class="main-card__item--item-info__top--title">{{$product->name}}</span>
                            <span class="main-card__item--item-info__top--subtitle">{{$product->vendor->name}}</span>
                        </div>
                        <div class="main-card__item--item-info__bottom">
                            <div class="main-card__item--item-info__bottom--img">
                                <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                <img src="{{ asset('img/Star 5.svg') }}" alt="star" />
                            </div>
                            <div class="main-card__item--item-info__bottom--cost">
                                {{ $product->current }} UZS
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
            <div class="main-title">Топ продаж за месяц
                <span class="red-item">
                    <a href="#"> Все <span class="tire">></span></a>
                </span>
            </div>
            <div class=" big-cards">
                @foreach($products as $key => $product)
{{--                <a href="#" class="main-card__item big-cards__item-bottom">--}}
{{--                    <div class="big-cards__item-bottom--prodaj">1 000 340 продаж</div>--}}
{{--                    <div class="big-cards__item-bottom--second-img"><img src="{{asset($product->image[0])}}" alt="photo" /></div>--}}
{{--                    <div class="main-card__item--item-info  big-cards__item-bottom_first">--}}
{{--                        <div class="main-card__item--item-info__top big-cards__item-bottom_first-top">--}}
{{--                            <span class="main-card__item--item-info__top--title big-cards__item-bottom_first-top__title">{{$product->name}} </span>--}}
{{--                            <span class="main-card__item--item-info__top--subtitle big-cards__item-bottom_first-top__subtitle">{{$product->vendor->name}}</span>--}}
{{--                        </div>--}}
{{--                        <div class="big-cards__item-bottom_first-bottom__wrapper">--}}
{{--                            <div class="main-card__item--item-info__bottom big-cards__item-bottom_first-bottom__left">--}}
{{--                                <div class="main-card__item--item-info__bottom--img">--}}
{{--                                    <img src="{{ asset('img/Star 3.svg') }}" alt="star" />--}}
{{--                                    <img src="{{ asset('img/Star 3.svg') }}" alt="star" />--}}
{{--                                    <img src="{{ asset('img/Star 3.svg') }}" alt="star" />--}}
{{--                                    <img src="{{ asset('img/Star 3.svg') }}" alt="star" />--}}
{{--                                    <img src="{{ asset('img/Star 5.svg') }}" alt="star" />--}}
{{--                                </div>--}}
{{--                                <div class="main-card__item--item-info__bottom--cost big-cards__item-bottom_first-bottom__cost">--}}
{{--                                    {{ $product->current }} UZS--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="main-card__item--item-info__bottom big-cards__item-bottom_first-bottom__right">--}}
{{--                                <div class="big-cards__item-bottom_first-bottom__right--img">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </a>--}}
                <a href="#" class="main-card__item big-cards__item-bottom">
                    <div class="big-cards__item-bottom--prodaj">1 000 340 продаж</div>
                    <div class="big-cards__item-bottom--second-img"><img src="{{asset($product->image[0])}}" alt="photo" /></div>
                    <div class="big-cards__item-bottom_wrapper">
                        <div class="main-card__item--item-info  big-cards__item-bottom_first">
                            <div class="main-card__item--item-info__top big-cards__item-bottom_first-top">
                                <span class="main-card__item--item-info__top--title big-cards__item-bottom_first-top__title">{{$product->name}} </span>
                                <span class="main-card__item--item-info__top--subtitle big-cards__item-bottom_first-top__subtitle">{{$product->vendor->name}}</span>
                            </div>
                            <div class="big-cards__item-bottom_first-bottom__wrapper">
                                <div class="main-card__item--item-info__bottom big-cards__item-bottom_first-bottom__left">
                                    <div class="main-card__item--item-info__bottom--img big-cards__item-bottom_first-bottom__left--img">
                                        <img src="img/Star 3.svg" alt="">
                                        <img src="img/Star 3.svg" alt="">
                                        <img src="img/Star 3.svg" alt="">
                                        <img src="img/Star 3.svg" alt="">
                                        <img src="img/Star 5.svg" alt="">
                                    </div>
                                    <div class="main-card__item--item-info__bottom--cost big-cards__item-bottom_first-bottom__cost">4 560 USD</div>
                                </div>
                            </div>
                        </div>
                        <div class="big-cards__item-bottom_two">
                            <div class="big-cards__item-bottom_two-img" style="background: url({{$product->vendor->image[0]}}) no-repeat center; background-size: cover;"></div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
            <div class="section-advert">
                <div class="section-advert-left">
                    <div class="section-advert-left__top">МЕСТО ДЛЯ ВАШЕЙ РЕКЛАМЫ</div>
                    <div class="section-advert-left__bottom">МЕСТО ДЛЯ ВАШЕЙ РЕКЛАМЫ</div>
                </div>
                <div class="section-advert-right"><div class="section-advert-right__content">МЕСТО ДЛЯ ВАШЕЙ РЕКЛАМЫ</div></div>
            </div>
            <div class="section-product">
                    <div>
                        <div class="main-title">Текстильные изделия <span class="red-item"><a href="#"> Все <span class="tire">></span></a></span></div>
                        <div class="main-card">
                            @foreach($products as $key => $product)
                            <a class="main-card__item" href="{{action('PageController@showProduct',[$product->vendor->alias,$product->alias])}}">
                                <div class="main-card__item--stock">
                                    <div class="main-card__item--stock_percent">-25%</div>
                                </div>
                                <div class="main-card__item--img" style="background: url({{asset($product->image[0])}}) no-repeat center; background-size: cover;"></div>
                                <div class="main-card__item--item-info">
                                    <div class="main-card__item--item-info__top">
                                        <span class="main-card__item--item-info__top--title">{{$product->name}}</span>
                                        <span class="main-card__item--item-info__top--subtitle">{{$product->vendor->name}}</span>
                                    </div>
                                    <div class="main-card__item--item-info__bottom">
                                        <div class="main-card__item--item-info__bottom--img">
                                            <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                            <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                            <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                            <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                            <img src="{{ asset('img/Star 5.svg') }}" alt="star" />
                                        </div>
                                        <div class="main-card__item--item-info__bottom--cost">
                                            {{ $product->current }} UZS
                                        </div>
                                    </div>
                                </div>
                            </a>
                            @endforeach
                        </div>
                        <div class="main-title">Бытовая техника<span class="red-item"><a href="#"> Все <span class="tire">></span></a></span></div>
                        <div class="main-card">
                            @foreach($products as $key => $product)
                            <a class="main-card__item" href="{{action('PageController@showProduct',[$product->vendor->alias,$product->alias])}}">
                                <div class="main-card__item--stock">
                                    <div class="main-card__item--stock_percent">-25%</div>
                                </div>
                                <div class="main-card__item--img" style="background: url({{asset($product->image[0])}}) no-repeat center; background-size: cover;"></div>
                                <div class="main-card__item--item-info">
                                    <div class="main-card__item--item-info__top">
                                        <span class="main-card__item--item-info__top--title">{{$product->name}}</span>
                                        <span class="main-card__item--item-info__top--subtitle">{{$product->vendor->name}}</span>
                                    </div>
                                    <div class="main-card__item--item-info__bottom">
                                        <div class="main-card__item--item-info__bottom--img">
                                            <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                            <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                            <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                            <img src="{{ asset('img/Star 3.svg') }}" alt="star" />
                                            <img src="{{ asset('img/Star 5.svg') }}" alt="star" />
                                        </div>
                                        <div class="main-card__item--item-info__bottom--cost">
                                            {{ $product->current }} UZS
                                        </div>
                                    </div>
                                </div>
                            </a>
                            @endforeach
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{asset('js/main.js')}}"></script>
@endsection
