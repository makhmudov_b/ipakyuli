@extends('layouts.frontend')

@section('content')
    <div class="container">
        <div class="main-title">
            {{$category->name ?? $keyword}}
        </div>
        <div class="filter">
            <div class="filter-wrapper">
{{--                <div class="filter-content">--}}
{{--                    <ul class="filter-content__ul">--}}
{{--                        <li><a href="{{action('PageController@category',[$category->alias,'price'])}}" class="active">По цене</a></li>--}}
{{--                        <li><a href="{{action('PageController@category',[$category->alias,'created_at'])}}">По новизне</a></li>--}}
{{--                        <li><a href="{{action('PageController@category',[$category->alias])}}">По дате</a></li>--}}
{{--                        <li><a href="{{action('PageController@category',$category->alias)}}">По продоваемости</a></li>--}}
{{--                        <li><a href="{{action('PageController@category',$category->alias)}}">По скидкам</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
            </div>
        </div>
        <div class="section-product">
            <div class="section-product-wrapper">
                <div class="section-product-card">
                    @foreach($products as $product)
                        @include('partials.product')
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
