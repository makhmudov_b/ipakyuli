@extends('layouts.frontend')

@section('content')
    <div class="collections-wrap auth-wrap d-flex align-items-center justify-content-center">
        <div class="container">
            <form class="auth" method="POST" action="{{action('AuthController@auth')}}">
                @csrf
                <img src="{{asset('img/logo.svg')}}" alt="logo">
                <div class="phone d-flex align-items-center justify-content-between">
                    <input type="hidden" name="phone" value="{{$phone}}" />
                    <input type="number" min="0" minlength="4" maxlength="4" name="password" placeholder="Введите отправленный код"  required="required"/>
                </div>
                <div class="rights">
                    СМС на +998 {{$phone}}
                </div>
                <button class="btn" type="submit">Продолжить</button>
            </form>
        </div>
    </div>
@endsection
