@extends('layouts.frontend')

@section('content')
    <br>
    <div class="category">
        <div class="container">
            <div class="category__all">
                {{$category->name}}
            </div>
            <div class="category__choice d-flex align-items-center justify-content-between flex-wrap">
                @foreach($category->sub_category as $item)
                    <a href="{{action('PageController@category',$item->alias)}}"
                       class="category__all btn">{{$item->name}}</a>
                @endforeach
            </div>
            <div class="section-product result d-flex flex-wrap">
                @foreach($products as $product)
                    @include('partials.product')
                @endforeach
            </div>
        </div>
    </div>
@endsection
