@extends('layouts.frontend')

@section('content')
    <basket :remove-basket-item="removeBasketItem"></basket>
@endsection
@section('script')
{{--<script src="{{ asset('backend/js/vendors/selectize.min.js') }}"></script>--}}
<script>
    Vue.component('basket', {
        props: ['removeBasketItem'],
        data() {
            return {
                items: [],
                fetchedItems: [],
                checkedItems: [],
                fullPrice: 0,
                countries: {!! $countries !!},
                cities: {!! $cities !!},
                country:0,
                city: 0
            }
        },
        mounted: function () {
            this.items = JSON.parse(localStorage.getItem('basket'));
            this.checkedItems = this.items;
            this.getData();
            let app = this;
            setTimeout(function(){
                app.checkedItems.map(e => {
                    app.fullPrice+= app.getPrice(e);
                    return 0;
                });
            },1000);
        },
        methods: {
            getData: function(){
                let data = {
                  products: localStorage.getItem('basket'),
                };
                let app = this;
                $.ajax({
                    type: 'get',
                    url: '{{action('PageController@basketProducts')}}',
                    data: data,
                    success: function(msg){
                        app.fetchedItems = msg;
                    }
                });
                return 1;
            },
            getProduct: function(productId){
                return this.fetchedItems.find(e => parseInt(e.id) === parseInt(productId));
            },
            resetCity: function() {
                this.city = 0;
                document.querySelector('#city_id').selectedIndex = 0;
            },
            getPrice: function(item){
              let findItem = this.fetchedItems.find(e => e.id === parseInt(item[1]));
              let getCountPriceDay = [];
              let price = JSON.parse(findItem.price);
              if(findItem.type === 'default') {
                  getCountPriceDay = price.filter(e => (parseInt(e[0]) <= parseInt(item[0])));
              }
              else{
                  let size = findItem.size.find(e => e.id === parseInt(item[2]));
                  let type = size.type.find(e => e.id === parseInt(item[3]));
                  price = JSON.parse(type.price);
                  getCountPriceDay = price.filter(e => (parseInt(e[0]) <= parseInt(item[0])));
              }
              if(getCountPriceDay.length < 1) {
                  getCountPriceDay = price[0];
              }
              else getCountPriceDay = getCountPriceDay[getCountPriceDay.length - 1];
                return parseInt(getCountPriceDay[1]) * parseInt(item[0]);
            },
            removeItem: function(productKey, count){
                let getBasket = localStorage.getItem('basket');
                getBasket = JSON.parse(getBasket);
                let filtered = getBasket.filter((e , key) => key !== productKey);
                if(getBasket){
                    if(getBasket.length > 0){
                        localStorage.setItem('basket',JSON.stringify(filtered));
                        this.items = filtered;
                        this.removeBasketItem(count);
                        $.notify("@lang('main.basket.0') ", "success");
                        return 1;
                    }
                }
                return 1;
            }
        },
        computed:{
            getRelatedCities(){
                return this.cities.filter(item => item.country_id == this.country);
            }
        },
        template: `
        <div class="main-section">
            <div class="container">
                <form action="{{action('OrderController@checkout')}}" method="POST" class="main-section-wrapper">
                    @csrf
                    <div class="main-section-process">
                        <div class="main-section-process-title">
                            <div class="main-section-process-title__count">@lang('main.basket.1')</div>
                            <div class="main-section-process-title__name adress-shipping">
                                <div class="main-section-process-title__name--img"><img src="{{ asset('img/Layer 2 (1).svg') }}" alt="">
                                </div>
                                <div class="main-section-process-title__name--title">@lang('main.basket.2')</div>
                            </div>
                        </div>
                        <div class="main-section-process-action current">
                            <div class="basket-choice d-flex flex-column">
                            <label for="city_id" class="current__adress--top_title">Страна</label>
                            <select name="country_id" id="country_id" v-model="country" @change="resetCity" class="form-control custom-select" required="required">
                                <option disabled>@lang('main.basket.4') </option>
                                <option v-for="(city , key) in countries" v-bind:value="city.id">@{{ city.name }}</option>
                            </select>
                            </div>
                            <div class="basket-choice d-flex flex-column">
                            <label for="city_id" class="current__adress--top_title">Город</label>
                            <select name="city_id" id="city_id" class="form-control custom-select" v-model="city" required="required">
                                <option disabled="disabled">@lang('main.basket.4') </option>
                                <option v-for="(city , key) in getRelatedCities" v-bind:value="city.id">@{{ city.name }}</option>
                            </select>
                            </div>
                        </div>
                        <div class="main-section-process-title">
                            <div class="main-section-process-title__count">@lang('main.basket.5')</div>
                            <div class="main-section-process-title__name">
                                <div class="main-section-process-title__name--img"><img src="../img/Layer 2 (2).svg" alt="">
                                </div>
                                <div class="main-section-process-title__name--title">@lang('main.basket.6')</div>
                            </div>
                        </div>
                        <div class="main-section-process-action two">
                            <div class="two-top">
                                <input type="radio" name="payment_method" value="bank" id="money1" checked="checked" />
                                <label for="money1">@lang('main.basket.7')</label>
{{--                            <input type="radio" name="payment_method" id="money2" />
                                <label for="money2">Наличными</label>
--}}
                            </div>
{{--                            <div class="two-middle">
                                <input type="radio" name="card" id="card1" checked="checked" />
                                <label class="two-middle__cards" for="card1">
                                    <img src="../img/Rectangle 40.png" alt="">
                                    <span class="two-middle__cards--name">Master Card</span>
                                </label>
                                <input type="radio" name="card" id="card2" />
                                <label class="two-middle__cards" for="card2">
                                    <img src="../img/Rectangle 40.png" alt="">
                                    <span class="two-middle__cards--name">Visa Card</span>
                                </label>
                                <input type="radio" name="card" id="card3" />
                                <label class="two-middle__cards" for="card3">
                                    <img src="../img/Rectangle 40.png" alt="">
                                    <span class="two-middle__cards--name">Yandex Денги</span>
                                </label>
                                <input type="radio" name="card" id="card4" />
                                <label class="two-middle__cards" for="card4">
                                    <img src="../img/Rectangle 40.png" alt="">
                                    <span class="two-middle__cards--name">UzCard</span>
                                </label>
                            </div>
                            --}}
                            <div class="two-bottom">
                                <div class="two-bottom__text">@lang('main.basket.8')</div>
                            </div>
                        </div>
                        <div class="main-section-process-title">
                            <div class="main-section-process-title__count">@lang('main.basket.9')</div>
                            <div class="main-section-process-title__name">
                                <div class="main-section-process-title__name--img"><img src="{{ asset('img/Layer 2 (1).png') }}" alt="">
                                </div>
                                <div class="main-section-process-title__name--title">@lang('main.basket.10') </div>
                            </div>
                        </div>
                        <div class="main-section-process-action third current">
                            <div class="third-wrapper justify-content-center" v-if="this.items.length == 0"> @lang('main.basket.11') </div>
                            <div class="third-wrapper" v-for="(item , key) in items" v-if="fetchedItems.length > 0">
                                <input type="checkbox" :id="'checkbox'+key" v-model="checkedItems" name="products[]" :value="item" />
                                <label :for="'checkbox'+key" class="third-item">
                                    <div class="third-item__img" v-bind:style='{ backgroundImage: "url(" + getProduct(item[1]).image[0] + ")", }'></div>
                                    <div class="third-item__description">
                                        <div class="third-item__description--info">
                                            <div class="item-info">
                                                <div class="item-info__top">
                                                    <div class="item-info__top--title">@{{ getProduct(item[1]).name }} </div>
                                                    <div class="item-info__top--subtitle">@{{ getProduct(item[1]).vendor.name }}</div>
                                                </div>
                                                <div class="item-info__bottom">
                                                    <div class="item-info__bottom--cost" style="margin-left:0;">@{{ getPrice(item) }} UZS</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="third-item__description--countity">
                                            <div class="third-item__description--countity_quantity">
                                                <div class="third-item__description--countity_quantity-title">Количество:
                                                </div>
                                                <div class="third-item__description--countity_quantity-navs">
                                                    {{--<button><img src="{{ asset('img/Group 55.svg') }}" alt=""></button>--}}
                                                    <div class="number">@{{ item[0] }}</div>
                                                    {{--<button><img src="{{ asset('img/Group 54.svg') }}" alt=""></button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="third-item__remove">
                                        <div class="third-item__remove--link"><button type="button" v-on:click="removeItem(key , item[0])"><img src="{{ asset('img/Layer 2 (3).svg') }}" alt=""></button></div>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="main-section-total-sums">
                        <div class="main-section-total-sums-title">
                            <div class="main-section-total-sums-title__name">
                                @lang('main.basket.12')
                            </div>
                        </div>
                        <div class="main-section-total-sums-info">
                            <div class="main-section-total-sums-info__top">
                                <div class="main-section-total-sums-info__top--left">
                                    {{--  <div class="main-section-total-sums-info__top--left_item">За доставку</div>--}}
                                    <div class="main-section-total-sums-info__top--left_item">@lang('main.basket.13')</div>
                                </div>
                                <div class="main-section-total-sums-info__top--right">
                                    {{-- <div class="main-section-total-sums-info__top--right_item">40 900 UZS</div> --}}
                                    <div class="main-section-total-sums-info__top--right_item">@{{ fullPrice }} UZS</div>
                                </div>
                            </div>
                            <div class="main-section-total-sums-info__bottom">
                                <div class="main-section-total-sums-info__bottom--pay">
                                    <div class="main-section-total-sums-info__bottom--pay_left">@lang('main.basket.14')</div>
                                    <div class="main-section-total-sums-info__bottom--pay_right">@{{ fullPrice }} UZS</div>
                                </div>
                                <div class="main-section-total-sums-info__bottom_btn">
                                    <button type="submit">@lang('main.basket.15')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div> `
    });
</script>
@endsection
