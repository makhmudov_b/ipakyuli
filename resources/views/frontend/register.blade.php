@extends('layouts.frontend')

@section('content')
    <div class="container">
        <div class="section-register">
            <div class="section-register-wrapper">
                <form action="{{action('AuthController@register')}}" method="POST" class="section-register-form">
                    @csrf
                    <div class="section-register-form__field first-field">
                    <legend class="section-register-form__field--legend">Личные данные</legend>
                        <div class="section-register-form__field--input">
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="name">Имя</label>
                                    <input type="text" name="name" id="name" />
                                </div>
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Фамилия</label>
                                    <input type="text" placeholder="">
                                </div>
                            </div>
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Логин</label>
                                    <input type="text" placeholder="">
                                </div>
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Email адрес</label>
                                    <input type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-register-form__field first-field">
                    <legend class="section-register-form__field--legend">Пароль</legend>
                        <div class="section-register-form__field--input">
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Город</label>
                                    <input type="text" placeholder="" id="">
                                </div>
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Страна</label>
                                    <input type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-register-form__field first-field">
                    <legend class="section-register-form__field--legend">Профиль</legend>
                        <div class="section-register-form__field--input">
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Город</label>
                                    <input type="text" placeholder="" id="">
                                </div>
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Страна</label>
                                    <input type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-register-form__field first-field">
                    <legend class="section-register-form__field--legend">Организация</legend>
                        <div class="section-register-form__field--input">
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Имя</label>
                                    <input type="text" placeholder="" id="" class="section-register-form__field--input-wrapper_grow">
                                </div>
                            </div>
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Логин</label>
                                    <input type="text" placeholder="">
                                </div>
                                <div class="section-register-form__field--input-wrapper ">
                                    <div class="section-register-form__submit section-register-form__field--input-wrapper_submit">
                                        <input type="button" value="Загрузить">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-register-form__field first-field">
                    <legend class="section-register-form__field--legend">Банковские данные</legend>
                        <div class="section-register-form__field--input">
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Банк</label>
                                    <input type="text" placeholder="" id="">
                                </div>
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="" class="section-register-form__field--input-wrapper_bank">Название Банка </label>
                                </div>
                            </div>
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Адрес</label>
                                    <input type="text" placeholder="" class="section-register-form__field--input-wrapper_grow">

                                </div>
                            </div>
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Р/С</label>
                                    <input type="text" placeholder="">
                                </div>

                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">ИНН</label>
                                    <input type="text" placeholder="">
                                </div>
                            </div>
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">МФО</label>
                                    <input type="text" placeholder="">
                                </div>


                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">СВИФТ</label>
                                    <input type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-register-form__field first-field">
                    <legend class="section-register-form__field--legend">Банк корреспонденты</legend>
                        <div class="section-register-form__field--input">
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">Банк</label>

                                    <input type="text" placeholder="" id="">
                                </div>
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="" class="section-register-form__field--input-wrapper_bank">Название Банк корреспондента  </label>

                                </div>
                            </div>
                            <div class="section-register-form__field--input_item">
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">СВИФТ</label>
                                    <input type="text" placeholder="">
                                </div>
                                <div class="section-register-form__field--input-wrapper wrapper-register">
                                    <label for="">№ счета</label>
                                    <input type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-register-form__submit">
                        <input type="button" value="Сохранить">
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
