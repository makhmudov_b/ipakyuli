@extends('layouts.frontend')

@section('content')
    <div class="container">
        <br>
        <a href="{{action('PageController@messages')}}">
            <div class="category__all">@lang('main.messages.1')</div>
        </a>
        <br>
        <div class="messages no-scroll" style="overflow: auto;">
            <div class="block">
                <div class="image"></div>
                <div class="info">
                    <div class="flex-1">
                        <div class="bold">№ {{$order->id}}</div>
                        <div class="bold m-10">{{$order->order_item[0]->name}}</div>
                    </div>
                    <div class="date">
                        <div class="bold">@lang('main.messages.2'): {{$order->price}} UZS</div>
                        {{--                        <div class="bold m-10">{{\Carbon\Carbon::parse($order->created_at)->format('d-m-yy')}}</div>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="order-items__wrap">
            @foreach($order->order_item as $item)
                <a href="{{action('PageController@showProduct',[$item->product->vendor->alias ,$item->product->alias] )}}" class="order-items d-flex align-items-center justify-content-between flex-wrap">
                    <div class="left">
                        <img src="{{$item->product->image[0]}}" width="100" alt="">
                    </div>
                    <div class="right d-flex align-items-center justify-content-between">
                        <div class="name">{{$item->product->name}}</div>
                        <div class="count">{{$item->amount}} {{ $item->product->count_type }}</div>
                        <div class="price">{{$item->price}} UZS</div>
                    </div>
                </a>
            @endforeach
        </div>
        <div class="messages-list">
            @foreach($messages as $message)
                @if($message->user_id != Auth::user()->id)
                    <div class="message-item another">
                        @if(count($message->image))
                            <a href="{{asset($message->image[0])}}" target="_blank">
                                <img src="{{asset($message->image[0])}}" alt="image">
                            </a>
                        @endif
                        <p>{{$message->body}} </p>
                    </div>
                @else
                    <div class="message-item own">
                        @if(count($message->image))
                            <a href="{{asset($message->image[0])}}" target="_blank">
                                <img src="{{asset($message->image[0])}}" alt="image">
                            </a>
                        @endif
                        <p>{{$message->body}}</p>
                    </div>
                @endif
            @endforeach
        </div>
        <form class="send" action="{{ action('MessageController@send') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="file" name="image">
            <input type="hidden" name="order_id" value="{{$order->id}}">
            <input type="text" required="required" class="mess" placeholder="@lang('main.messages.3')" name="body"/>
            <button class="button" type="submit">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0)">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M6.13527 18.2512L8.75934 13.0023L16.6344 13.0016L6.13527 18.2512ZM16.6343 11.0019L8.75994 11.0019L6.13516 5.7523L16.6343 11.0019ZM22.0162 12.8933C22.0802 12.8217 22.1328 12.7432 22.1726 12.6562C22.1876 12.626 22.2011 12.5958 22.2123 12.5633C22.2537 12.4488 22.2809 12.3289 22.2782 12.2008C22.2754 12.0727 22.2431 11.9514 22.1959 11.8343C22.184 11.8021 22.17 11.772 22.1537 11.7411C22.1086 11.6508 22.0525 11.5685 21.9839 11.4926C21.9679 11.4751 21.9563 11.4563 21.9388 11.4388C21.8613 11.3642 21.7743 11.2976 21.6764 11.2463L3.61363 1.9269C3.20272 1.71562 2.71595 1.78405 2.40191 2.09808C2.08788 2.41212 2.01872 2.89961 2.23073 3.3098L6.64428 11.8648L2.59458 20.2393C2.40076 20.64 2.49018 21.1302 2.81801 21.458C3.14583 21.7859 3.636 21.8753 4.03671 21.6815L21.7154 13.1302C21.8119 13.0824 21.8961 13.0209 21.9698 12.9488C21.988 12.9321 21.9995 12.9115 22.0162 12.8933Z"
                              fill="#999999"/>
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="24" height="24" fill="white"/>
                        </clipPath>
                    </defs>
                </svg>
            </button>
        </form>
    </div>
    <br><br>
@endsection
