@extends('layouts.frontend')

@section('content')
    <br><br>
    <div class="container">
        <form action="{{action('PageController@search')}}"
              class="search-wrap d-flex align-items-center justify-content-between flex-wrap">
            <input type="text" placeholder="Что вы ищете ?" value="{{$keyword}}" class="input" name="keyword"/>
            <div class="select">
                <select name="category_id" id="category_id">
                    <option>Категория</option>
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}"
                                @if($category->id == $category_id) selected @endif>{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="select">
                <select name="vendor_id" id="vendor_id">
                    <option>Производитель</option>
                    @foreach ($vendors as $vendor)
                        <option value="{{$vendor->id}}"
                                @if($vendor->id == $vendor_id) selected @endif>{{$vendor->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="d-flex align-items-center justify-content-between buttons">
                <a href="{{action('PageController@search')}}" class="btn danger-btn">Сбросить</a>
                <button type="submit" class="btn main-btn">Применить</button>
            </div>
        </form>
        @if(!count($products))
            <div class="not-found d-flex flex-column align-items-center justify-content-center">
                <img src="{{asset('img/logo-3d.svg')}}" alt="logo-3d">
                <div class="info-title">По вашему запросу ничего не найдено</div>
            </div>
        @else
            <div class="section-product result d-flex flex-wrap">
                @foreach($products as $product)
                    @include('partials.product')
                @endforeach
            </div>
        @endif
        <div class="d-flex align-items-center justify-content-end">
            {{ $products->links() }}
        </div>
    </div>
    <br><br>
@endsection
