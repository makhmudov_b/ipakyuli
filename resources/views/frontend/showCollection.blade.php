@extends('layouts.frontend')

@section('content')
<div class="collections-wrap">
    <div class="container">
        <div class="category__all">
            {{$category->name}}
        </div>
        <div class="section-product result d-flex flex-wrap">
            @foreach($products as $product)
                @include('partials.product')
            @endforeach
        </div>
    </div>
</div>
@endsection
