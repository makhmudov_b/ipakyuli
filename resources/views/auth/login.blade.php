<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <title>IpakYuli - Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <script src="{{ asset('backend/js/require.min.js')}}"></script>
    <script>
      requirejs.config({
          baseUrl: '/'
      });
    </script>
    <link href="{{ asset('backend/css/dashboard.css')}}" rel="stylesheet" />
    <link href="{{ asset('backend/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/js/dashboard.js')}}"></script>
    <link href="{{ asset('backend/plugins/charts-c3/plugin.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/plugins/charts-c3/plugin.js')}}"></script>
    <link href="{{ asset('backend/plugins/maps-google/plugin.css')}}" rel="stylesheet" />
    <script src="{{ asset('backend/plugins/maps-google/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/input-mask/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/datatables/plugin.js') }}"></script>
{{--    <script src="{{ asset('backend/js/custom.js')}}"></script>--}}
    <link href="{{ asset('backend/css/custom.css')}}" rel="stylesheet" />
    <link href="{{ asset('backend/css/style.css')}}" rel="stylesheet" />

  </head>
  <body>
	<section class="authorization">
		<div class="container">
			<div class="row">
				<form action="{{ route('login') }}" method="POST" autocomplete="off">
                    @csrf
                    <div class="white-block form">
						<div class="top-block text-center">
							<a href="#" class="form-logo">
								<img src="{{ asset('backend/images/logo.svg') }}" alt="">
							</a>
							<h3 class="form-title">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
								Вход
							</h3>
						</div>
						<div class="content">
							<div class="form-input">
								<label for="email">E-mail:</label>
								<input type="email" id="email" name="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}">
							</div>
							<div class="form-input between">
								<label for="password">Пароль:</label>
								<a href="/password/reset" class="restore">Восстановить</a>
								<input type="password" id="password" name="password">
							</div>
							<div class="button-block">
								<button type="submit" class="continue-btn">Войти</button>
							</div>
						</div>
					</div>
					<div class="bottom-text">
						Выполняя вход или регистрируясь, вы соглашаетесь <a href="#">с правилами и условиями использования.</a>
					</div>
				</form>
			</div>
		</div>
	</section>

  </body>
</html>
