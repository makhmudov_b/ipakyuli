@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form method="POST" action="{{ route('register') }}">
                    <div class="white-block">
                        @csrf
                        <div class="head">
                            <h3>@lang('main.register.0')</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label>@lang('main.register.1'):</label>
                                    <input type="text"
                                           class="form-control regStepOne {{ $errors->has('name') ? ' isNotValidInput' : '' }}"
                                           name="name" value="{{ old('name') }}" id="regUserName"
                                           aria-describedby="emailHelp" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="text-block">

                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>E-mail:</label>
                                    <input type="email" name="email"
                                           class="form-control regStepOne {{ $errors->has('email') ? ' isNotValidInput' : '' }}"
                                           id="regUserEmail" value="{{ old('email') }}" required
                                           aria-describedby="emailHelp"/>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="text-block">

                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="regUserPhone">@lang('main.register.2')</label>
                                    <div class="phone">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-phone phone-icon">
                                            <path
                                                d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                        </svg>
                                        <input type="number" name="phone" id="regUserPhone"
                                               class="regStepOne forPhone {{ $errors->has('phone') ? ' isNotValidInput' : '' }}"
                                               value="{{ old('phone') }}" required="required"/>
                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="text-block">
                                    <p>@lang('main.register.3')</p>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>@lang('main.register.4'):</label>
                                    <input type="password" name="password" class="form-control regStepOne"
                                           id="regUserPass" aria-describedby="emailHelp" placeholder="">
                                </div>
                                <div class="text-block">

                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label>@lang('main.register.5'):</label>
                                    <input type="password" name="password_confirmation" class="form-control regStepOne"
                                           id="regUserConfirmPass" aria-describedby="emailHelp" placeholder="">
                                </div>
                                <div class="text-block">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">@lang('main.register.6')</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
