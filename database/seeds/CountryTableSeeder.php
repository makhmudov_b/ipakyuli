<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Country::create([
            'name_ru'=>'Узбекистан',
            'name_uz'=>'Uzbekistan',
            'flag'=>'uz',
        ]);
    }
}
