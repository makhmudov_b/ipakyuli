<?php

use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\City::create([
            'name_ru'=>'Ташкент',
            'name_uz'=>'Tashkent',
            'country_id'=> 1,
        ]);
    }
}
