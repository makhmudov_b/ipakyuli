<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Category::create([
            'name_ru'=>'Електроника',
            'name_uz'=>'Електроника',
            'description_ru'=>'Tashkent',
            'description_uz'=>'Tashkent'
        ]);
    }
}
