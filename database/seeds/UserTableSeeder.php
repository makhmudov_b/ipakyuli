<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('type', 'admin')->first();
        $admin = new User;
        $admin->name = 'admin';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('secret');
        $admin->phone = '998990008991';
        $admin->save();
        $admin->roles()->attach($role_admin);
        $role_manager = Role::where('type', 'manager')->first();
        $admin = new User;
        $admin->name = 'manager';
        $admin->email = 'manager@gmail.com';
        $admin->vendor_id = 1;
        $admin->password = bcrypt('secret');
        $admin->phone = '998990008991';
        $admin->save();
        $admin->roles()->attach($role_manager);
    }
}
