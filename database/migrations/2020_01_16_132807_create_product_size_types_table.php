<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSizeTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_size_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('product_size_id');
            $table->string('name_uz');
            $table->string('name_ru');
            $table->string('alias');
            $table->unsignedInteger('amount')->nullable();
            $table->longText('price');
            $table->string('currency');
            $table->unsignedInteger('physical_price')->nullable();
            $table->boolean('available')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_size_types');
    }
}
