<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('vendor_id');
            $table->longText('price');
            $table->integer('min_price')->default(100000);
            $table->integer('min_count')->default(0);
            $table->string('count_type')->default('шт');
            $table->unsignedInteger('physical_price')->nullable();
            $table->unsignedInteger('price_dropper')->default(0);
            $table->string('name_uz');
            $table->string('name_ru');
            $table->string('alias');
            $table->string('currency');
            $table->string('guarantee')->nullable();
            $table->longText('description_uz');
            $table->longText('description_ru');
            $table->boolean('enabled')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
