<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('vendor_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('carrier_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->string('phone')->nullable();
            $table->unsignedInteger('price');
            $table->unsignedInteger('status')->default(0);
            $table->integer('paid')->default(0);
            $table->unsignedInteger('payment_method')->default(0);
            $table->unsignedInteger('shipping_price')->default(0);
            $table->string('carrier')->nullable();
            $table->text('comment')->nullable();
            $table->string('address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
